(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["all-users-update-profile-update-profile-module"],{

/***/ "./src/app/components/all-users/update-profile/update-profile-routing.module.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/components/all-users/update-profile/update-profile-routing.module.ts ***!
  \**************************************************************************************/
/*! exports provided: UpdateProfileRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProfileRoutingModule", function() { return UpdateProfileRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _update_profile_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./update-profile.component */ "./src/app/components/all-users/update-profile/update-profile.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _update_profile_component__WEBPACK_IMPORTED_MODULE_2__["UpdateProfileComponent"]
    }
];
var UpdateProfileRoutingModule = /** @class */ (function () {
    function UpdateProfileRoutingModule() {
    }
    UpdateProfileRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], UpdateProfileRoutingModule);
    return UpdateProfileRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/all-users/update-profile/update-profile.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/components/all-users/update-profile/update-profile.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container\" style=\"padding-top: 60px;\">\n    <h1 class=\"page-header\">Edit Profile</h1>\n    <div class=\"row\">\n        <!-- left column -->\n        <div class=\"col-md-4 col-sm-6 col-xs-12\">\n            <div class=\"text-center\">\n                <img src=\"{{this.profile_image}}\" class=\"avatar img-circle img-thumbnail\" alt=\"avatar\">\n                <input type=\"file\" class=\"text-center center-block well well-sm\"\n                       (change)=\"imageFinishedUploading($event);preview(file.files);\">\n            </div>\n        </div>\n        <!-- edit form column -->\n        <div class=\"col-md-8 col-sm-6 col-xs-12 personal-info\">\n            <h2>Personal info</h2>\n            <form [formGroup]=\"updateProfile\" enctype=\"multipart/form-data\" >\n                <div class=\"form-group\">\n                    <label class=\"col-lg-3 control-label\">First name:</label>\n                    <div class=\"col-lg-8\">\n                        <input class=\"form-control\"  formControlName=\"fname\" type=\"text\" [(ngModel)]=\"model.fname\">\n                    </div>\n                    <div *ngIf=\"submitted && f.fname.errors\" style=\"color: red\">\n                        <div *ngIf=\"f.fname.errors.required\">First Name is required</div>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-lg-3 control-label\">Last name:</label>\n                    <div class=\"col-lg-8\">\n                        <input class=\"form-control\" formControlName=\"lname\"  type=\"text\" [(ngModel)]=\"model.lname\">\n                    </div>\n                    <div *ngIf=\"submitted && f.lname.errors\" style=\"color: red\">\n                        <div *ngIf=\"f.lname.errors.required\">Last Name is required</div>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-lg-3 control-label\">Email:</label>\n                    <div class=\"col-lg-8\">\n                        <input class=\"form-control\" formControlName=\"email\" type=\"text\" [(ngModel)]=\"model.email\">\n                    </div>\n                    <div *ngIf=\"submitted && f.email.errors\" style=\"color: red\">\n                        <div *ngIf=\"f.email.errors.required\">Email Name is required</div>\n                    </div>\n                </div>\n                <div class=\"form-group\">\n                    <label class=\"col-md-3 control-label\">Username:</label>\n                    <div class=\"col-md-8\">\n                        <input class=\"form-control\" formControlName=\"name\" type=\"text\" [(ngModel)]=\"model.name\" disabled>\n                    </div>\n                    <div *ngIf=\"submitted && f.name.errors\" style=\"color: red\">\n                        <div *ngIf=\"f.name.errors.required\">User Name is required</div>\n                    </div>\n                </div>\n\n                <div class=\"form-group\" >\n                    <label class=\"col-md-3 control-label\"></label>\n                    <div class=\"col-md-8\" style=\"margin-top: 20px;\">\n                        <input type=\"submit\" class=\"btn btn-primary\" value=\"Save Changes\" (click)=\"onSubmit()\">\n                        <span></span>\n                        <input class=\"btn btn-default\" value=\"Cancel\" type=\"reset\">\n                    </div>\n                </div>\n            </form>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/components/all-users/update-profile/update-profile.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/components/all-users/update-profile/update-profile.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWxsLXVzZXJzL3VwZGF0ZS1wcm9maWxlL3VwZGF0ZS1wcm9maWxlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/all-users/update-profile/update-profile.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/all-users/update-profile/update-profile.component.ts ***!
  \*********************************************************************************/
/*! exports provided: UpdateProfileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateProfileComponent", function() { return UpdateProfileComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _shared_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/authentication.service */ "./src/app/shared/services/authentication.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UpdateProfileComponent = /** @class */ (function () {
    function UpdateProfileComponent(modalService, formBuilder, route, authenticationService, router) {
        this.modalService = modalService;
        this.formBuilder = formBuilder;
        this.route = route;
        this.authenticationService = authenticationService;
        this.router = router;
        // public imagePath;
        // imgURL: any = 'assets/images/user.png';
        this.model = {};
        this.users = [];
        this.imagedata = [];
        this.images = [];
        this.loading = false;
        this.submitted = false;
        this.showImg = false;
        this.updateProfile = this.formBuilder.group({
            fname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            lname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            name: [''],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required]
        });
    }
    Object.defineProperty(UpdateProfileComponent.prototype, "f", {
        get: function () { return this.updateProfile.controls; },
        enumerable: true,
        configurable: true
    });
    UpdateProfileComponent.prototype.ngOnInit = function () {
        this.userDetails();
        this.profile_image = localStorage.getItem('profile_photo');
    };
    UpdateProfileComponent.prototype.userDetails = function () {
        var _this = this;
        this.authenticationService.get('details')
            .subscribe(function (response) {
            _this.users = response['result'];
            _this.model.email = response['result'].email;
            _this.model.fname = response['result'].fname;
            _this.model.lname = response['result'].lname;
            _this.model.name = response['result'].name;
            localStorage.setItem('profile_photo', response['result'].profile_photo);
        }, function (error) {
            _this.loading = false;
        });
    };
    UpdateProfileComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        if (this.updateProfile.invalid) {
            return;
        }
        this.loading = true;
        var fd = new FormData();
        fd.append('fname', this.model.fname);
        fd.append('lname', this.model.lname);
        for (var i = 0; i < this.imagedata.length; i++) {
            fd.append('profile_image', this.imagedata[i]);
        }
        this.authenticationService.post('updateProfile', fd)
            .subscribe(function (response) {
            _this.userDetails();
        }, function (error) {
            _this.loading = false;
        });
    };
    UpdateProfileComponent.prototype.imageFinishedUploading = function (event) {
        var _this = this;
        this.showImg = true;
        for (var i = 0; i < event.target.files.length; i++) {
            var name_1 = 'mis_images[' + i + ']';
            this.getBase64(event.target.files[i]).then(function (data) {
                _this.images.push(data);
            });
            this.imagedata.push(event.target.files[i]);
        }
    };
    UpdateProfileComponent.prototype.getBase64 = function (file) {
        return new Promise(function (resolve, reject) {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () { return resolve(reader.result); };
            reader.onerror = function (error) { return reject(error); };
        });
    };
    UpdateProfileComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-update-profile',
            template: __webpack_require__(/*! ./update-profile.component.html */ "./src/app/components/all-users/update-profile/update-profile.component.html"),
            styles: [__webpack_require__(/*! ./update-profile.component.scss */ "./src/app/components/all-users/update-profile/update-profile.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_1__["BsModalService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            _shared_services_authentication_service__WEBPACK_IMPORTED_MODULE_2__["AuthenticationService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], UpdateProfileComponent);
    return UpdateProfileComponent;
}());



/***/ }),

/***/ "./src/app/components/all-users/update-profile/update-profile.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/components/all-users/update-profile/update-profile.module.ts ***!
  \******************************************************************************/
/*! exports provided: UdateProfileModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UdateProfileModule", function() { return UdateProfileModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _update_profile_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./update-profile-routing.module */ "./src/app/components/all-users/update-profile/update-profile-routing.module.ts");
/* harmony import */ var _update_profile_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./update-profile.component */ "./src/app/components/all-users/update-profile/update-profile.component.ts");
/* harmony import */ var _shared_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../shared/index */ "./src/app/shared/index.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var UdateProfileModule = /** @class */ (function () {
    function UdateProfileModule() {
    }
    UdateProfileModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCarouselModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbAlertModule"],
                _update_profile_routing_module__WEBPACK_IMPORTED_MODULE_4__["UpdateProfileRoutingModule"],
                _shared_index__WEBPACK_IMPORTED_MODULE_6__["StatModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]
            ],
            declarations: [
                _update_profile_component__WEBPACK_IMPORTED_MODULE_5__["UpdateProfileComponent"]
            ]
        })
    ], UdateProfileModule);
    return UdateProfileModule;
}());



/***/ })

}]);
//# sourceMappingURL=all-users-update-profile-update-profile-module.js.map