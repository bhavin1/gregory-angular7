(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-auth-signup-signup-module"],{

/***/ "./src/app/components/auth/signup/signup-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/auth/signup/signup-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: SignupRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupRoutingModule", function() { return SignupRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _signup_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./signup.component */ "./src/app/components/auth/signup/signup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _signup_component__WEBPACK_IMPORTED_MODULE_2__["SignupComponent"]
    }
];
var SignupRoutingModule = /** @class */ (function () {
    function SignupRoutingModule() {
    }
    SignupRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], SignupRoutingModule);
    return SignupRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/auth/signup/signup.component.html":
/*!**************************************************************!*\
  !*** ./src/app/components/auth/signup/signup.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"login-page\" [@routerTransition]>\n    <div class=\"row justify-content-md-center\">\n        <div class=\"col-md-4\">\n            <img class=\"user-avatar\" src=\"assets/images/sample_logo.png\" width=\"150px\" style=\"background-color: white;\"/>\n            <h1>Sign Up</h1>\n            <form [formGroup]=\"signup\">\n                <div class=\"form-content\" >\n                    <div class=\"form-group\">\n                        <input type=\"text\" formControlName=\"fname\" class=\"form-control input-underline input-lg\" id=\"fname\"\n                               placeholder=\"{{ 'First Name' | translate }}\" [(ngModel)]=\"model.fname\"\n                               [ngClass]=\"{ 'is-invalid': submitted && f.fname.errors }\"/>\n                        <div *ngIf=\"submitted && f.fname.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.fname.errors.required\">First name is required</div>\n                        </div>\n                    </div>\n                    <div class=\"form-group\">\n                        <input type=\"text\" formControlName=\"lname\" class=\"form-control input-underline input-lg\" id=\"lname\"\n                               placeholder=\"{{ 'Last Name' | translate }}\" [(ngModel)]=\"model.lname\"\n                               [ngClass]=\"{ 'is-invalid': submitted && f.lname.errors }\"/>\n                        <div *ngIf=\"submitted && f.lname.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.lname.errors.required\">Last name is required</div>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                        <input type=\"text\" formControlName=\"email\" class=\"form-control input-underline input-lg\" id=\"email\"\n                               placeholder=\"{{ 'Email' | translate }}\" [(ngModel)]=\"model.email\"\n                               [ngClass]=\"{ 'is-invalid': submitted && f.email.errors }\"/>\n                        <div *ngIf=\"submitted && f.email.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.email.errors.required\">Email is required</div>\n                        </div>\n                    </div>\n                    <!--  <div class=\"form-group\">\n                        <input type=\"number\" formControlName=\"mobile\" class=\"form-control input-underline input-lg\" id=\"mobile\"   \n                               placeholder=\"{{ 'Contact Number' | translate }}\" [(ngModel)]=\"model.mobile\"\n                               [ngClass]=\"{ 'is-invalid': submitted && f.mobile.errors }\"/>\n                        <div *ngIf=\"submitted && f.mobile.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.mobile.errors.required\">Contact Number is required</div> -->\n                            <!-- <div *ngIf=\"f.mobile.errors.maxlength\">Contact Number should not be more than 10 digits</div> -->\n                        <!-- </div>\n                    </div> -->\n\n                    <div class=\"form-group\">\n                        <input type=\"password\" formControlName=\"password\" class=\"form-control input-underline input-lg\"\n                               id=\"password\" placeholder=\"{{ 'Password' | translate }}\" [(ngModel)]=\"model.password\"\n                               [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\"/>\n                        <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.password.errors.required\">Password is required</div>\n                        </div>\n                    </div>\n\n                    <div class=\"form-group\">\n                    <input type=\"password\" formControlName=\"c_password\" class=\"form-control input-underline input-lg\"\n                           id=\"c_password\" placeholder=\"{{ 'Re-enter Password' | translate }}\" [(ngModel)]=\"model.c_password\"\n                           [ngClass]=\"{ 'is-invalid': submitted && f.c_password.errors }\"/>\n                    <div *ngIf=\"submitted && f.c_password.errors\" class=\"invalid-feedback\">\n                        <div  *ngIf=\"f.c_password.errors.required\">Re-Password is required</div>\n                        <div *ngIf=\"f.c_password.errors.mustMatch\">Passwords must match</div>\n                    </div>\n                   </div>\n\n                    <div class=\"form-group\">\n                            <select name=\"role\" formControlName=\"role\"\n                                    [ngClass]=\"{ 'is-invalid': submitted && f.role.errors }\"\n                                    [(ngModel)]=\"model.role\"  class=\"btn btn-sm dropdown-toggle role-dropdown\"\n                                     >\n                                <option [ngValue]=\"undefined\" disabled  selected>Please Select Role</option>\n                                <option *ngFor=\"let role of roles\" value=\"{{role.id}}\">{{role.name}}</option>\n                            </select>\n                        <div *ngIf=\"submitted && f.role.errors\" class=\"invalid-feedback\">\n                            <div *ngIf=\"f.role.errors.required\">Role is required</div>\n                        </div>\n                    </div>\n\n                </div>\n                <a class=\"btn rounded-btn\" (click)=\"onSubmit()\">{{ 'Register' | translate }}</a>&nbsp;\n                <a class=\"btn rounded-btn\" [routerLink]=\"['/login']\">{{ 'Log in' | translate }}</a>\n            </form>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/auth/signup/signup.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/components/auth/signup/signup.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host {\n  display: block; }\n\n.login-page {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  overflow: auto;\n  background: #131522;\n  text-align: center;\n  color: #fff;\n  padding: 3em; }\n\n.login-page .col-lg-4 {\n    padding: 0; }\n\n.login-page .input-lg {\n    height: 46px;\n    padding: 10px 16px;\n    font-size: 18px;\n    line-height: 1.3333333;\n    border-radius: 0; }\n\n.login-page .input-underline {\n    background: 0 0;\n    border: none;\n    box-shadow: none;\n    border-bottom: 2px solid rgba(255, 255, 255, 0.5);\n    color: #fff;\n    border-radius: 0; }\n\n.login-page .input-underline:focus {\n    border-bottom: 2px solid #fff;\n    box-shadow: none; }\n\n.login-page .rounded-btn {\n    border-radius: 50px;\n    color: rgba(255, 255, 255, 0.8);\n    background: #131522;\n    border: 2px solid rgba(255, 255, 255, 0.8);\n    font-size: 18px;\n    line-height: 40px;\n    padding: 0 25px; }\n\n.login-page .rounded-btn:hover,\n  .login-page .rounded-btn:focus,\n  .login-page .rounded-btn:active,\n  .login-page .rounded-btn:visited {\n    color: white;\n    border: 2px solid white;\n    outline: none; }\n\n.login-page h1 {\n    font-weight: 300;\n    margin-top: 20px;\n    margin-bottom: 10px;\n    font-size: 36px; }\n\n.login-page h1 small {\n      color: rgba(255, 255, 255, 0.7); }\n\n.login-page .form-group {\n    padding: 8px 0; }\n\n.login-page .form-group input::-webkit-input-placeholder {\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input:-moz-placeholder {\n      /* Firefox 18- */\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input::-moz-placeholder {\n      /* Firefox 19+ */\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-group input:-ms-input-placeholder {\n      color: rgba(255, 255, 255, 0.6) !important; }\n\n.login-page .form-content {\n    padding: 40px 0; }\n\n.login-page .user-avatar {\n    border-radius: 50%;\n    border: 2px solid #fff; }\n\n.role-dropdown {\n  width: 100%;\n  height: 46px;\n  font-size: 18px;\n  color: #949494;\n  background-color: transparent;\n  border-bottom: 2px solid rgba(255, 255, 255, 0.5); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvZG9jdW1lbnRzL1NCLUFkbWluLUJTNC1Bbmd1bGFyLTYtbWFzdGVyL3NyYy9hcHAvY29tcG9uZW50cy9hdXRoL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5zY3NzIiwiL3Zhci93d3cvaHRtbC9kb2N1bWVudHMvU0ItQWRtaW4tQlM0LUFuZ3VsYXItNi1tYXN0ZXIvc3JjL2FwcC9jb21wb25lbnRzL2F1dGgvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLGVBQWMsRUFDakI7O0FBQ0Q7RUFDSSxtQkFBa0I7RUFDbEIsT0FBTTtFQUNOLFFBQU87RUFDUCxTQUFRO0VBQ1IsVUFBUztFQUNULGVBQWM7RUFDZCxvQkFYNkI7RUFZN0IsbUJBQWtCO0VBQ2xCLFlBQVc7RUFDWCxhQUFZLEVBZ0ZmOztBQTFGRDtJQVlRLFdBQVUsRUFDYjs7QUFiTDtJQWVRLGFBQVk7SUFDWixtQkFBa0I7SUFDbEIsZ0JBQWU7SUFDZix1QkFBc0I7SUFDdEIsaUJBQWdCLEVBQ25COztBQXBCTDtJQXNCUSxnQkFBZTtJQUNmLGFBQVk7SUFDWixpQkFBZ0I7SUFDaEIsa0RBQWlEO0lBQ2pELFlBQVc7SUFDWCxpQkFBZ0IsRUFDbkI7O0FBNUJMO0lBOEJRLDhCQUE2QjtJQUM3QixpQkFBZ0IsRUFDbkI7O0FBaENMO0lBbUNRLG9CQUFtQjtJQUNuQixnQ0FBK0I7SUFDL0Isb0JBekN5QjtJQTBDekIsMkNBQTBDO0lBQzFDLGdCQUFlO0lBQ2Ysa0JBQWlCO0lBQ2pCLGdCQUFlLEVBQ2xCOztBQTFDTDs7OztJQStDUSxhQUE2QjtJQUM3Qix3QkFBd0M7SUFDeEMsY0FBYSxFQUNoQjs7QUFsREw7SUFxRFEsaUJBQWdCO0lBQ2hCLGlCQUFnQjtJQUNoQixvQkFBbUI7SUFDbkIsZ0JBQWUsRUFJbEI7O0FBNURMO01BMERZLGdDQUErQixFQUNsQzs7QUEzRFQ7SUErRFEsZUFBYyxFQWtCakI7O0FBakZMO01BaUVZLDJDQUEwQyxFQUM3Qzs7QUFsRVQ7TUFxRVksaUJBQWlCO01BQ2pCLDJDQUEwQyxFQUM3Qzs7QUF2RVQ7TUEwRVksaUJBQWlCO01BQ2pCLDJDQUEwQyxFQUM3Qzs7QUE1RVQ7TUErRVksMkNBQTBDLEVBQzdDOztBQWhGVDtJQW1GUSxnQkFBZSxFQUNsQjs7QUFwRkw7SUF1RlEsbUJBQWtCO0lBQ2xCLHVCQUFzQixFQUN6Qjs7QUMxRkw7RUFDQyxZQUFXO0VBQ1IsYUFBWTtFQUNaLGdCQUFlO0VBQ2YsZUFBYztFQUNkLDhCQUE2QjtFQUM3QixrREFBaUQsRUFDcEQiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2F1dGgvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjogIzEzMTUyMjtcbjpob3N0IHtcbiAgICBkaXNwbGF5OiBibG9jaztcbn1cbi5sb2dpbi1wYWdlIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIG92ZXJmbG93OiBhdXRvO1xuICAgIGJhY2tncm91bmQ6ICR0b3BuYXYtYmFja2dyb3VuZC1jb2xvcjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgcGFkZGluZzogM2VtO1xuICAgIC5jb2wtbGctNCB7XG4gICAgICAgIHBhZGRpbmc6IDA7XG4gICAgfVxuICAgIC5pbnB1dC1sZyB7XG4gICAgICAgIGhlaWdodDogNDZweDtcbiAgICAgICAgcGFkZGluZzogMTBweCAxNnB4O1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjMzMzMzMzM7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgfVxuICAgIC5pbnB1dC11bmRlcmxpbmUge1xuICAgICAgICBiYWNrZ3JvdW5kOiAwIDA7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC41KTtcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XG4gICAgfVxuICAgIC5pbnB1dC11bmRlcmxpbmU6Zm9jdXMge1xuICAgICAgICBib3JkZXItYm90dG9tOiAycHggc29saWQgI2ZmZjtcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICB9XG4gICAgLnJvdW5kZWQtYnRuIHtcbiAgICAgICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1MHB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICAgICAgICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjgpO1xuICAgICAgICBiYWNrZ3JvdW5kOiAkdG9wbmF2LWJhY2tncm91bmQtY29sb3I7XG4gICAgICAgIGJvcmRlcjogMnB4IHNvbGlkIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC44KTtcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICBsaW5lLWhlaWdodDogNDBweDtcbiAgICAgICAgcGFkZGluZzogMCAyNXB4O1xuICAgIH1cbiAgICAucm91bmRlZC1idG46aG92ZXIsXG4gICAgLnJvdW5kZWQtYnRuOmZvY3VzLFxuICAgIC5yb3VuZGVkLWJ0bjphY3RpdmUsXG4gICAgLnJvdW5kZWQtYnRuOnZpc2l0ZWQge1xuICAgICAgICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAxKTtcbiAgICAgICAgYm9yZGVyOiAycHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAxKTtcbiAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICB9XG5cbiAgICBoMSB7XG4gICAgICAgIGZvbnQtd2VpZ2h0OiAzMDA7XG4gICAgICAgIG1hcmdpbi10b3A6IDIwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMzZweDtcbiAgICAgICAgc21hbGwge1xuICAgICAgICAgICAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5mb3JtLWdyb3VwIHtcbiAgICAgICAgcGFkZGluZzogOHB4IDA7XG4gICAgICAgIGlucHV0Ojotd2Via2l0LWlucHV0LXBsYWNlaG9sZGVyIHtcbiAgICAgICAgICAgIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNikgIWltcG9ydGFudDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlucHV0Oi1tb3otcGxhY2Vob2xkZXIge1xuICAgICAgICAgICAgLyogRmlyZWZveCAxOC0gKi9cbiAgICAgICAgICAgIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNikgIWltcG9ydGFudDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlucHV0OjotbW96LXBsYWNlaG9sZGVyIHtcbiAgICAgICAgICAgIC8qIEZpcmVmb3ggMTkrICovXG4gICAgICAgICAgICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpICFpbXBvcnRhbnQ7XG4gICAgICAgIH1cblxuICAgICAgICBpbnB1dDotbXMtaW5wdXQtcGxhY2Vob2xkZXIge1xuICAgICAgICAgICAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KSAhaW1wb3J0YW50O1xuICAgICAgICB9XG4gICAgfVxuICAgIC5mb3JtLWNvbnRlbnQge1xuICAgICAgICBwYWRkaW5nOiA0MHB4IDA7XG4gICAgfVxuICAgIC51c2VyLWF2YXRhciB7XG4gICAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgICAgIGJvcmRlcjogMnB4IHNvbGlkICNmZmY7XG4gICAgfVxufVxuIiwiLy8gc2hhcmVkIGNzcyBmb3IgdGhlIGxvZ2luIGFuZCBzaWdudXAgcGFnZVxuQGltcG9ydCBcIi4uL2xvZ2luL2xvZ2luLmNvbXBvbmVudFwiO1xuXG4ucm9sZS1kcm9wZG93bntcblx0d2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA0NnB4O1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBjb2xvcjogIzk0OTQ5NDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXItYm90dG9tOiAycHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjUpO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/components/auth/signup/signup.component.ts":
/*!************************************************************!*\
  !*** ./src/app/components/auth/signup/signup.component.ts ***!
  \************************************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/services/authentication.service */ "./src/app/shared/services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SignupComponent = /** @class */ (function () {
    function SignupComponent(translate, router, formBuilder, route, authenticationService) {
        this.translate = translate;
        this.router = router;
        this.formBuilder = formBuilder;
        this.route = route;
        this.authenticationService = authenticationService;
        this.model = {};
        this.alert = {};
        this.loading = false;
        this.submitted = false;
        this.error = '';
        this.roles = [{
                id: 2,
                name: "Arichtect"
            }, {
                id: 3,
                name: "Landlord"
            }, {
                id: 4,
                name: "Broker"
            }];
        // this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        // this.translate.setDefaultLang('en');
        var browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
        this.signup = this.formBuilder.group({
            fname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            lname: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            role: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            c_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required]
        }, {
            validator: this.MustMatch('password', 'c_password')
        });
    }
    SignupComponent.prototype.MustMatch = function (controlName, matchingControlName) {
        return function (formGroup) {
            var control = formGroup.controls[controlName];
            var matchingControl = formGroup.controls[matchingControlName];
            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }
            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            }
            else {
                matchingControl.setErrors(null);
            }
        };
    };
    SignupComponent.prototype.ngOnInit = function () {
        // reset login status
        this.authenticationService.logout();
        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    };
    Object.defineProperty(SignupComponent.prototype, "f", {
        get: function () { return this.signup.controls; },
        enumerable: true,
        configurable: true
    });
    SignupComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        if (this.signup.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.post('register', this.model)
            .subscribe(function (response) {
            console.log(response);
            if (response['status'] == 1) {
                _this.loading = false;
                localStorage.setItem('currentUser', response['result']['token']);
                localStorage.setItem('user_role', response['result']['user']['role']);
                localStorage.setItem('user_id', response['result']['user']['id']);
                localStorage.setItem('user_name', response['result']['user']['name']);
                localStorage.setItem('fname', response['result']['user']['fname']);
                localStorage.setItem('lname', response['result']['user']['lname']);
                localStorage.setItem('email', response['result']['user']['email']);
                localStorage.setItem('isLoggedin', 'true'); // check login user
                _this.router.navigate(['dashboard4']);
            }
            else {
                _this.loading = false;
                _this.alert.success = true;
                _this.alert.alert_type = 'alert-danger';
                _this.alert.title = 'Error';
                _this.alert.title = response['msg'];
            }
        }, function (error) {
            _this.loading = false;
        });
    };
    SignupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-signup',
            template: __webpack_require__(/*! ./signup.component.html */ "./src/app/components/auth/signup/signup.component.html"),
            styles: [__webpack_require__(/*! ./signup.component.scss */ "./src/app/components/auth/signup/signup.component.scss")],
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()]
        }),
        __metadata("design:paramtypes", [_ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _shared_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], SignupComponent);
    return SignupComponent;
}());



/***/ }),

/***/ "./src/app/components/auth/signup/signup.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/auth/signup/signup.module.ts ***!
  \*********************************************************/
/*! exports provided: SignupModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupModule", function() { return SignupModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var _signup_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup-routing.module */ "./src/app/components/auth/signup/signup-routing.module.ts");
/* harmony import */ var _signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signup.component */ "./src/app/components/auth/signup/signup.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var SignupModule = /** @class */ (function () {
    function SignupModule() {
    }
    SignupModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateModule"],
                _signup_routing_module__WEBPACK_IMPORTED_MODULE_3__["SignupRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"]
            ],
            declarations: [_signup_component__WEBPACK_IMPORTED_MODULE_4__["SignupComponent"]]
        })
    ], SignupModule);
    return SignupModule;
}());



/***/ })

}]);
//# sourceMappingURL=components-auth-signup-signup-module.js.map