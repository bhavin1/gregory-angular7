(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["add-building-add-building-module"],{

/***/ "./src/app/components/add-building/add-building.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/add-building/add-building.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<h2 class=\"text-muted\">Add New Building</h2>\n<hr>\n<div class=\"jumbotron\" style=\"background-color: #d4d5d5;\" >\n\n    <form [formGroup]=\"addBuildingForm\">\n        <div class=\"row\">\n            Add New Building\n        </div>\n        <hr>\n        <div class=\"row\">\n            <div class=\"col-md-1\">\n                <div draggable=\"true\" ngClass=\"{{dragAreaClass}}\" style=\"width: 100px;height: 120px\">\n                    <div class=\"row\">\n                        <div class=\"col-md-12 text-center\">\n                            <a href=\"javascript:void(0)\" (click)=\"file.click()\">\n                                <!--<i class=\"fa fa-building fa-2x\" aria-hidden=\"true\" style=\"color: #343a40\"></i>-->\n                                <img [src]=\"imgURL\" width=\"50px\" height=\"50px\">\n                            </a>\n                            <input type=\"file\" #file\n                                   (change)=\"onFileChange($event)\" style=\"display:none\"/>\n                        </div>\n                    </div>\n                </div>\n            </div>\n\n            <div class=\"col-md-3\">\n                <label for=\"building_name\">Building Name *</label><br>\n                <input type=\"text\" formControlName=\"building_name\" id=\"building_name\"\n                       placeholder=\"{{ 'building_name' | translate }}\" [(ngModel)]=\"model.building_name\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.building_name.errors }\"/>\n                <div *ngIf=\"submitted && f.building_name.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.building_name.errors.required\">Building Name is required</div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <label for=\"owner_name\">Owner Name *</label><br>\n                <input type=\"text\" formControlName=\"owner_name\" id=\"owner_name\"\n                       placeholder=\"{{ 'owner_name' | translate }}\" [(ngModel)]=\"model.owner_name\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.owner_name.errors }\"/>\n                <div *ngIf=\"submitted && f.owner_name.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.owner_name.errors.required\">Building Name is required</div>\n                </div>\n            </div>\n            <div class=\"col-md-3\">\n                <label for=\"owner_contact\">Owner Contact *</label><br>\n                <input type=\"text\" formControlName=\"owner_contact\" id=\"owner_contact\"\n                       [(ngModel)]=\"model.owner_contact\" placeholder=\"{{ 'owner_contact' | translate }}\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.owner_contact.errors }\"/>\n                <div *ngIf=\"submitted && f.owner_contact.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.owner_contact.errors.required\">Building Name is required</div>\n                </div>\n            </div>\n            <div class=\"col-md-2\">\n                <label for=\"floor_counts\">Floor Count *</label><br>\n                <input type=\"text\" formControlName=\"floor_counts\" id=\"floor_counts\"\n                       [(ngModel)]=\"model.floor_counts\" placeholder=\"{{ 'floor_counts' | translate }}\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.floor_counts.errors }\"/>\n                <div *ngIf=\"submitted && f.floor_counts.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.floor_counts.errors.required\">Building Name is required</div>\n                </div>\n            </div>\n        </div>\n        <br>\n        <div class=\"row\">\n            Building Address\n        </div>\n        <hr>\n        <div class=\"row\">\n            <div class=\"col-md-2\">\n                <label for=\"building_address\">Building Address Line *</label><br>\n                <input type=\"text\" formControlName=\"building_address\" id=\"building_address\"\n                       [(ngModel)]=\"model.building_address\" placeholder=\"{{ 'building_address' | translate }}\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.building_address.errors }\"/>\n                <div *ngIf=\"submitted && f.building_address.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.building_address.errors.required\">Building Address is required</div>\n                </div>\n            </div>\n            <div class=\"col-md-2\">\n                <label for=\"city\"> City *</label><br>\n                <input type=\"text\" formControlName=\"city\" id=\"city\"\n                       [(ngModel)]=\"model.city\" placeholder=\"{{ 'city' | translate }}\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.city.errors }\"/>\n                <div *ngIf=\"submitted && f.city.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.city.errors.required\">City is required</div>\n                </div>\n            </div>\n            <div class=\"col-md-2\">\n                <label for=\"state\"> State *</label><br>\n                <input type=\"text\" formControlName=\"state\" id=\"state\"\n                       [(ngModel)]=\"model.state\" placeholder=\"{{ 'state' | translate }}\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.state.errors }\"/>\n                <div *ngIf=\"submitted && f.state.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.state.errors.required\">State is required</div>\n                </div>\n            </div>\n            <div class=\"col-md-2\">\n                <label for=\"zip_code\">Zip Code *</label><br>\n                <input type=\"text\" formControlName=\"zip_code\" id=\"zip_code\"\n                       [(ngModel)]=\"model.zip_code\" placeholder=\"{{ 'zip_code' | translate }}\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.zip_code.errors }\"/>\n                <div *ngIf=\"submitted && f.zip_code.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.zip_code.errors.required\">Zip Code is required</div>\n                </div>\n            </div>\n            <div class=\"col-md-2\">\n                <label for=\"country\">Country *</label><br>\n                <input type=\"text\" formControlName=\"country\" id=\"country\"\n                       [(ngModel)]=\"model.country\" placeholder=\"{{ 'country' | translate }}\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.country.errors }\"/>\n                <div *ngIf=\"submitted && f.country.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.country.errors.required\">Country is required</div>\n                </div>\n            </div>\n            <div class=\"col-md-2\">\n                <label for=\"coordinates\">Coordinates *</label><br>\n                <input type=\"text\" formControlName=\"coordinates\" id=\"coordinates\"\n                       [(ngModel)]=\"model.coordinates\" placeholder=\"{{ 'coordinates' | translate }}\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.coordinates.errors }\"/>\n                <div *ngIf=\"submitted && f.coordinates.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.coordinates.errors.required\">Coordinates is required</div>\n                </div>\n            </div>\n        </div>\n        <br>\n\n        <div class=\"row\">\n            Floor Description\n        </div>\n        <hr>\n        <div class=\"row\">\n            <div class=\"col-md-2\">\n                <label for=\"floor_description\">Floor Description *</label><br>\n                <input type=\"text\" formControlName=\"floor_description\" id=\"floor_description\"\n                       [(ngModel)]=\"model.floor_description\" placeholder=\"{{ 'floor_description' | translate }}\"\n                       [ngClass]=\"{ 'is-invalid': submitted && f.floor_description.errors }\"/>\n                <div *ngIf=\"submitted && f.floor_description.errors\" style=\"color: red\">\n                    <div *ngIf=\"f.floor_description.errors.required\">Building Name is required</div>\n                </div>\n            </div>\n            <div class=\"col-md-2\">\n                <br>\n                <input type=\"file\" (change)=\"fileChange($event)\" />\n            </div>\n            <div class=\"col-md-2\"><br>\n                <button type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-plus\"></i>\n                </button>&nbsp;Add More\n            </div>\n        </div>\n        <hr>\n        <button type=\"submit\" class=\"btn btn-default display-4\" (click)=\"onSubmit()\">Submit</button>\n    </form>\n</div>\n"

/***/ }),

/***/ "./src/app/components/add-building/add-building.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/add-building/add-building.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#div1, #div2 {\n  float: left;\n  width: 100px;\n  height: 35px;\n  margin: 10px;\n  padding: 10px;\n  border: 1px solid black; }\n\n.error {\n  color: #f00; }\n\n.dragarea {\n  font-size: 24px;\n  border: 3px solid #bbb;\n  padding: 20px;\n  background-color: #fff;\n  color: #1c7430; }\n\n.droparea {\n  font-size: 24px;\n  border: 3px dashed #bbb;\n  padding: 20px;\n  background-color: #eff;\n  color: #0b2e13; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvd3d3L2h0bWwvZG9jdW1lbnRzL1NCLUFkbWluLUJTNC1Bbmd1bGFyLTYtbWFzdGVyL3NyYy9hcHAvY29tcG9uZW50cy9hZGQtYnVpbGRpbmcvYWRkLWJ1aWxkaW5nLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsWUFBVztFQUNYLGFBQVk7RUFDWixhQUFZO0VBQ1osYUFBWTtFQUNaLGNBQWE7RUFDYix3QkFBdUIsRUFDeEI7O0FBRUQ7RUFBUSxZQUFXLEVBQUk7O0FBQ3ZCO0VBQ0UsZ0JBQWU7RUFDZix1QkFBc0I7RUFDdEIsY0FBYztFQUNkLHVCQUFzQjtFQUN0QixlQUFjLEVBQ2Y7O0FBQ0Q7RUFDRSxnQkFBZTtFQUNmLHdCQUF1QjtFQUN2QixjQUFjO0VBQ2QsdUJBQXNCO0VBQ3RCLGVBQWMsRUFDZiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWRkLWJ1aWxkaW5nL2FkZC1idWlsZGluZy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuI2RpdjEsICNkaXYyIHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiAzNXB4O1xuICBtYXJnaW46IDEwcHg7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xufVxuXG4uZXJyb3J7IGNvbG9yOiAjZjAwOyB9XG4uZHJhZ2FyZWF7XG4gIGZvbnQtc2l6ZTogMjRweDtcbiAgYm9yZGVyOiAzcHggc29saWQgI2JiYjtcbiAgcGFkZGluZzogMjBweCA7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gIGNvbG9yOiAjMWM3NDMwO1xufVxuLmRyb3BhcmVhe1xuICBmb250LXNpemU6IDI0cHg7XG4gIGJvcmRlcjogM3B4IGRhc2hlZCAjYmJiO1xuICBwYWRkaW5nOiAyMHB4IDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VmZjtcbiAgY29sb3I6ICMwYjJlMTM7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/add-building/add-building.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/add-building/add-building.component.ts ***!
  \*******************************************************************/
/*! exports provided: AddBuildingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddBuildingComponent", function() { return AddBuildingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _shared_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../shared/services/authentication.service */ "./src/app/shared/services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddBuildingComponent = /** @class */ (function () {
    function AddBuildingComponent(modalService, formBuilder, route, authenticationService, translate, router) {
        this.modalService = modalService;
        this.formBuilder = formBuilder;
        this.route = route;
        this.authenticationService = authenticationService;
        this.translate = translate;
        this.router = router;
        this.fileToUpload = null;
        this.imgURL = 'assets/images/sample_building.jpg';
        this.dragAreaClass = 'dragarea';
        this.imagedata = [];
        this.images = [];
        this.showImg = false;
        this.model = {};
        this.alert = {};
        this.loading = false;
        this.submitted = false;
        this.error = '';
        this.formData = new FormData();
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        var browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
    }
    AddBuildingComponent.prototype.ngOnInit = function () {
        this.addBuildingForm = this.formBuilder.group({
            building_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            owner_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            owner_contact: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            floor_counts: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            building_address: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            city: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            state: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            zip_code: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            country: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            coordinates: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
            floor_description: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required],
        });
    };
    Object.defineProperty(AddBuildingComponent.prototype, "f", {
        get: function () { return this.addBuildingForm.controls; },
        enumerable: true,
        configurable: true
    });
    AddBuildingComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        if (this.addBuildingForm.invalid) {
            return;
        }
        this.formData.append('building_name', this.model.building_name);
        this.formData.append('owner_name', this.model.owner_name);
        this.formData.append('owner_contact', this.model.owner_contact);
        this.formData.append('floor_counts', this.model.floor_counts);
        this.formData.append('city', this.model.city);
        this.formData.append('state', this.model.state);
        this.formData.append('zip_code', this.model.zip_code);
        this.formData.append('country', this.model.country);
        this.formData.append('coordinates', this.model.coordinates);
        this.formData.append('floor_description', this.model.floor_description);
        this.loading = true;
        this.authenticationService.post('addBuilding', this.formData)
            .subscribe(function (response) {
            console.log(response);
        }, function (error) {
            _this.loading = false;
        });
    };
    AddBuildingComponent.prototype.onFileChange = function (event) {
        var files = event.target.files;
        console.log(files);
    };
    AddBuildingComponent.prototype.onDragOver = function (event) {
        this.dragAreaClass = "droparea";
        event.preventDefault();
    };
    AddBuildingComponent.prototype.onDragEnter = function (event) {
        this.dragAreaClass = "droparea";
        event.preventDefault();
    };
    AddBuildingComponent.prototype.onDragEnd = function (event) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
    };
    AddBuildingComponent.prototype.onDragLeave = function (event) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
    };
    AddBuildingComponent.prototype.onDrop = function (event) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
        event.stopPropagation();
        var files = event.dataTransfer.files;
        this.saveFiles(files);
    };
    AddBuildingComponent.prototype.saveFiles = function (files) {
        this.preview(files);
        if (files.length > 0) {
            for (var j = 0; j < files.length; j++) {
                this.formData.append("building_image", files[j], files[j].name);
            }
        }
    };
    AddBuildingComponent.prototype.preview = function (files) {
        var _this = this;
        if (files.length === 0)
            return;
        var mimeType = files[0].type;
        if (mimeType.match(/image\/*/) == null) {
            this.message = "Only images are supported.";
            return;
        }
        var reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = function (_event) {
            _this.imgURL = reader.result;
        };
    };
    // add card file code
    AddBuildingComponent.prototype.fileChange = function (event) {
        var _this = this;
        var fileList = event.target.files;
        if (fileList.length > 0) {
            var file = fileList[0];
            var formData1 = new FormData();
            formData1.append('cadd_file', file, file.name);
            var headers = new Headers();
            /** In Angular 5, including the header Content-Type can invalidate your request */
            headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');
            // let options = new RequestOptions({ headers: headers });
            this.authenticationService.post("convertBuilding", formData1)
                .subscribe(function (response) {
                if (response['msg'] === 'Success') {
                    _this.returnUrl = response['urn'];
                }
            }, function (error) {
                _this.loading = false;
            });
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('dragover', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AddBuildingComponent.prototype, "onDragOver", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('dragenter', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AddBuildingComponent.prototype, "onDragEnter", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('dragend', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AddBuildingComponent.prototype, "onDragEnd", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('dragleave', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AddBuildingComponent.prototype, "onDragLeave", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('drop', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AddBuildingComponent.prototype, "onDrop", null);
    AddBuildingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-add-building',
            template: __webpack_require__(/*! ./add-building.component.html */ "./src/app/components/add-building/add-building.component.html"),
            styles: [__webpack_require__(/*! ./add-building.component.scss */ "./src/app/components/add-building/add-building.component.scss")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_3__["BsModalService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _shared_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_2__["TranslateService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AddBuildingComponent);
    return AddBuildingComponent;
}());



/***/ }),

/***/ "./src/app/components/add-building/add-building.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/components/add-building/add-building.module.ts ***!
  \****************************************************************/
/*! exports provided: AddBuildingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddBuildingModule", function() { return AddBuildingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _add_building_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./add_building-routing.module */ "./src/app/components/add-building/add_building-routing.module.ts");
/* harmony import */ var _add_building_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-building.component */ "./src/app/components/add-building/add-building.component.ts");
/* harmony import */ var _shared_index__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../shared/index */ "./src/app/shared/index.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/fesm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var AddBuildingModule = /** @class */ (function () {
    function AddBuildingModule() {
    }
    AddBuildingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCarouselModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbAlertModule"],
                _add_building_routing_module__WEBPACK_IMPORTED_MODULE_4__["AddBuildingRoutingModule"],
                _shared_index__WEBPACK_IMPORTED_MODULE_6__["StatModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_7__["TranslateModule"]
            ],
            declarations: [
                _add_building_component__WEBPACK_IMPORTED_MODULE_5__["AddBuildingComponent"],
            ]
        })
    ], AddBuildingModule);
    return AddBuildingModule;
}());



/***/ }),

/***/ "./src/app/components/add-building/add_building-routing.module.ts":
/*!************************************************************************!*\
  !*** ./src/app/components/add-building/add_building-routing.module.ts ***!
  \************************************************************************/
/*! exports provided: AddBuildingRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddBuildingRoutingModule", function() { return AddBuildingRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _add_building_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./add-building.component */ "./src/app/components/add-building/add-building.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _add_building_component__WEBPACK_IMPORTED_MODULE_2__["AddBuildingComponent"]
    }
];
var AddBuildingRoutingModule = /** @class */ (function () {
    function AddBuildingRoutingModule() {
    }
    AddBuildingRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AddBuildingRoutingModule);
    return AddBuildingRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=add-building-add-building-module.js.map