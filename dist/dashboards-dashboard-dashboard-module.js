(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["dashboards-dashboard-dashboard-module"],{

/***/ "./src/app/components/dashboards/dashboard/dashboard-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/components/dashboards/dashboard/dashboard-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: DashboardRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardRoutingModule", function() { return DashboardRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/components/dashboards/dashboard/dashboard.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    {
        path: '', component: _dashboard_component__WEBPACK_IMPORTED_MODULE_2__["DashboardComponent"]
    }
];
var DashboardRoutingModule = /** @class */ (function () {
    function DashboardRoutingModule() {
    }
    DashboardRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], DashboardRoutingModule);
    return DashboardRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/dashboards/dashboard/dashboard.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/components/dashboards/dashboard/dashboard.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [@routerTransition]>\n    <h2 class=\"text-muted\">Admin Dashboard</h2> <hr>\n    <!--<div class=\"row\">-->\n        <!--<div class=\"col-md-12\">-->\n            <!--<ngb-carousel>-->\n                <!--<ng-template ngbSlide *ngFor=\"let slider of sliders\">-->\n                    <!--<img class=\"img-fluid mx-auto d-block\" [src]=\"slider.imagePath\" alt=\"Random first slide\" width=\"100%\">-->\n                    <!--<div class=\"carousel-caption\">-->\n                        <!--<h3>{{slider.label}}</h3>-->\n                        <!--<p>{{slider.text}}</p>-->\n                    <!--</div>-->\n                <!--</ng-template>-->\n            <!--</ngb-carousel>-->\n        <!--</div>-->\n    <!--</div>-->\n    <div class=\"row\">\n        <div class=\"col-xl-3 col-lg-6\">\n            <app-stat [bgClass]=\"'primary'\" [icon]=\"'fa-comments'\" [count]=\"26\" [label]=\"'Architect'\" ></app-stat>\n        </div>\n        <div class=\"col-xl-3 col-lg-6\">\n            <app-stat [bgClass]=\"'warning'\" [icon]=\"'fa-tasks'\" [count]=\"12\" [label]=\"'Landlord'\" ></app-stat>\n        </div>\n        <div class=\"col-xl-3 col-lg-6\">\n            <app-stat [bgClass]=\"'success'\" [icon]=\"'fa-shopping-cart'\" [count]=\"24\" [label]=\"'Broker'\" ></app-stat>\n        </div>\n        <div class=\"col-xl-3 col-lg-6\">\n            <app-stat [bgClass]=\"'danger'\" [icon]=\"'fa-support'\" [count]=\"113\" [label]=\"'Total Users'\"></app-stat>\n        </div>\n    </div>\n\n    <hr />\n    <ngb-alert [type]=\"alert.type\" (close)=\"closeAlert(alert)\" *ngFor=\"let alert of alerts\">{{ alert.message }}</ngb-alert>\n    <hr />\n\n   <div class=\"col-md-12\">\n    <div class=\"row\">\n        <div class=\"card col-md-6\" >\n            <div class=\"card-header border-transparent\">\n                <h3 class=\"card-title\">Building</h3>\n\n                <div class=\"card-tools\">\n                    <button type=\"button\" class=\"btn btn-tool\" data-widget=\"collapse\">\n                        <i class=\"fa fa-minus\"></i>\n                    </button>\n                    <button type=\"button\" class=\"btn btn-tool\" data-widget=\"remove\">\n                        <i class=\"fa fa-times\"></i>\n                    </button>\n                </div>\n            </div>\n            <!-- /.card-header -->\n            <div class=\"card-body p-0\">\n                <div class=\"table-responsive\">\n                    <table class=\"table m-0\">\n                        <thead>\n                        <tr>\n                            <th>Building ID</th>\n                            <th>Item</th>\n                            <th>Status</th>\n                            <th>Popularity</th>\n                        </tr>\n                        </thead>\n                        <tbody>\n                        <tr>\n                            <td><a href=\"pages/examples/invoice.html\">OR9842</a></td>\n                            <td>Call of Duty IV</td>\n                            <td><span class=\"badge badge-success\">Shipped</span></td>\n                            <td>\n                                <div class=\"sparkbar\" data-color=\"#00a65a\" data-height=\"20\">90,80,90,-70,61,-83,63</div>\n                            </td>\n                        </tr>\n                        <tr>\n                            <td><a href=\"pages/examples/invoice.html\">OR1848</a></td>\n                            <td>Samsung Smart TV</td>\n                            <td><span class=\"badge badge-warning\">Pending</span></td>\n                            <td>\n                                <div class=\"sparkbar\" data-color=\"#f39c12\" data-height=\"20\">90,80,-90,70,61,-83,68</div>\n                            </td>\n                        </tr>\n                        <tr>\n                            <td><a href=\"pages/examples/invoice.html\">OR7429</a></td>\n                            <td>iPhone 6 Plus</td>\n                            <td><span class=\"badge badge-danger\">Delivered</span></td>\n                            <td>\n                                <div class=\"sparkbar\" data-color=\"#f56954\" data-height=\"20\">90,-80,90,70,-61,83,63</div>\n                            </td>\n                        </tr>\n                        <tr>\n                            <td><a href=\"pages/examples/invoice.html\">OR7429</a></td>\n                            <td>Samsung Smart TV</td>\n                            <td><span class=\"badge badge-info\">Processing</span></td>\n                            <td>\n                                <div class=\"sparkbar\" data-color=\"#00c0ef\" data-height=\"20\">90,80,-90,70,-61,83,63</div>\n                            </td>\n                        </tr>\n                        <tr>\n                            <td><a href=\"pages/examples/invoice.html\">OR1848</a></td>\n                            <td>Samsung Smart TV</td>\n                            <td><span class=\"badge badge-warning\">Pending</span></td>\n                            <td>\n                                <div class=\"sparkbar\" data-color=\"#f39c12\" data-height=\"20\">90,80,-90,70,61,-83,68</div>\n                            </td>\n                        </tr>\n                        <tr>\n                            <td><a href=\"pages/examples/invoice.html\">OR7429</a></td>\n                            <td>iPhone 6 Plus</td>\n                            <td><span class=\"badge badge-danger\">Delivered</span></td>\n                            <td>\n                                <div class=\"sparkbar\" data-color=\"#f56954\" data-height=\"20\">90,-80,90,70,-61,83,63</div>\n                            </td>\n                        </tr>\n                        <tr>\n                            <td><a href=\"pages/examples/invoice.html\">OR9842</a></td>\n                            <td>Call of Duty IV</td>\n                            <td><span class=\"badge badge-success\">Shipped</span></td>\n                            <td>\n                                <div class=\"sparkbar\" data-color=\"#00a65a\" data-height=\"20\">90,80,90,-70,61,-83,63</div>\n                            </td>\n                        </tr>\n                        </tbody>\n                    </table>\n                </div>\n                <!-- /.table-responsive -->\n            </div>\n            <!-- /.card-body -->\n            <div class=\"card-footer clearfix\">\n                <a href=\"javascript:void(0)\" class=\"btn btn-sm btn-info float-left\">Place New Order</a>\n                <a href=\"javascript:void(0)\" class=\"btn btn-sm btn-secondary float-right\">View All Orders</a>\n            </div>\n        </div>\n        <div class=\"col-md-6\">\n            <!-- USERS LIST -->\n            <div class=\"card\">\n                <div class=\"card-header\">\n                    <h3 class=\"card-title\">Latest Members</h3>\n\n                    <div class=\"card-tools\">\n                        <span class=\"badge badge-danger\">8 New Members</span>\n                        <button type=\"button\" class=\"btn btn-tool\" data-widget=\"collapse\"><i class=\"fa fa-minus\"></i>\n                        </button>\n                        <button type=\"button\" class=\"btn btn-tool\" data-widget=\"remove\"><i class=\"fa fa-times\"></i>\n                        </button>\n                    </div>\n                </div>\n                <!-- /.card-header -->\n                <div class=\"card-body p-0\">\n                    <ul class=\"users-list clearfix\">\n                        <li>\n                            <img [src]=\"'assets/images/user1-128x128.jpg'\" alt=\"User Image\">\n                            <a class=\"users-list-name\" href=\"#\">Alexander Pierce</a>\n                            <span class=\"users-list-date\">Today</span>\n                        </li>\n                        <li>\n                            <img [src]=\"'assets/images/user8-128x128.jpg'\" alt=\"User Image\">\n                            <a class=\"users-list-name\" href=\"#\">Norman</a>\n                            <span class=\"users-list-date\">Yesterday</span>\n                        </li>\n                        <li>\n                            <img [src]=\"'assets/images/user7-128x128.jpg'\" alt=\"User Image\">\n                            <a class=\"users-list-name\" href=\"#\">Jane</a>\n                            <span class=\"users-list-date\">12 Jan</span>\n                        </li>\n                        <li>\n                            <img [src]=\"'assets/images/user6-128x128.jpg'\" alt=\"User Image\">\n                            <a class=\"users-list-name\" href=\"#\">John</a>\n                            <span class=\"users-list-date\">12 Jan</span>\n                        </li>\n                        <li>\n                            <img [src]=\"'assets/images/user2-160x160.jpg'\" alt=\"User Image\">\n                            <a class=\"users-list-name\" href=\"#\">Alexander</a>\n                            <span class=\"users-list-date\">13 Jan</span>\n                        </li>\n                        <li>\n                            <img [src]=\"'assets/images/user5-128x128.jpg'\" alt=\"User Image\">\n                            <a class=\"users-list-name\" href=\"#\">Sarah</a>\n                            <span class=\"users-list-date\">14 Jan</span>\n                        </li>\n                        <li>\n                            <img [src]=\"'assets/images/user4-128x128.jpg'\" alt=\"User Image\">\n                            <a class=\"users-list-name\" href=\"#\">Nora</a>\n                            <span class=\"users-list-date\">15 Jan</span>\n                        </li>\n                        <li>\n                            <img [src]=\"'assets/images/user3-128x128.jpg'\" alt=\"User Image\">\n                            <a class=\"users-list-name\" href=\"#\">Nadia</a>\n                            <span class=\"users-list-date\">15 Jan</span>\n                        </li>\n                    </ul>\n                    <!-- /.users-list -->\n                </div>\n                <!-- /.card-body -->\n                <div class=\"card-footer text-center\">\n                    <a href=\"javascript::\">View All Users</a>\n                </div>\n                <!-- /.card-footer -->\n            </div>\n            <!--/.card -->\n        </div>\n    </div>\n   </div>\n\n    <div class=\"raw\">\n        <section class=\"col-lg-7 connectedSortable\">\n            <div class=\"card\">\n                <div class=\"card-header d-flex p-0\">\n                    <h3 class=\"card-title p-3\">\n                        <i class=\"fa fa-pie-chart mr-1\"></i>\n                        Floor Plan\n                    </h3>\n                    <ul class=\"nav nav-pills ml-auto p-2\">\n                        <li class=\"nav-item\">\n                            <a class=\"nav-link active\" href=\"#revenue-chart\" data-toggle=\"tab\">Area</a>\n                        </li>\n                    </ul>\n                </div>\n                <div class=\"card-body\">\n                    <div class=\"chart tab-pane active\" id=\"revenue-chart\" style=\"position: relative; height: 300px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"><svg height=\"300\" version=\"1.1\" width=\"863.156\" xmlns=\"http://www.w3.org/2000/svg\" style=\"overflow: hidden; position: relative; left: -0.5px;\"><desc style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">Created with Raphaël 2.1.0</desc><defs style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></defs><text x=\"51.515625\" y=\"261\" text-anchor=\"end\" font=\"10px &quot;Arial&quot;\" stroke=\"none\" fill=\"#888888\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;\" font-size=\"12px\" font-family=\"sans-serif\" font-weight=\"normal\"><tspan dy=\"4\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">0</tspan></text><path fill=\"none\" stroke=\"#aaaaaa\" d=\"M64.015625,261H838.156\" stroke-width=\"0.5\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></path><text x=\"51.515625\" y=\"202\" text-anchor=\"end\" font=\"10px &quot;Arial&quot;\" stroke=\"none\" fill=\"#888888\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;\" font-size=\"12px\" font-family=\"sans-serif\" font-weight=\"normal\"><tspan dy=\"4\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">7,500</tspan></text><path fill=\"none\" stroke=\"#aaaaaa\" d=\"M64.015625,202H838.156\" stroke-width=\"0.5\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></path><text x=\"51.515625\" y=\"143\" text-anchor=\"end\" font=\"10px &quot;Arial&quot;\" stroke=\"none\" fill=\"#888888\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;\" font-size=\"12px\" font-family=\"sans-serif\" font-weight=\"normal\"><tspan dy=\"4\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">15,000</tspan></text><path fill=\"none\" stroke=\"#aaaaaa\" d=\"M64.015625,143H838.156\" stroke-width=\"0.5\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></path><text x=\"51.515625\" y=\"84.00000000000003\" text-anchor=\"end\" font=\"10px &quot;Arial&quot;\" stroke=\"none\" fill=\"#888888\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;\" font-size=\"12px\" font-family=\"sans-serif\" font-weight=\"normal\"><tspan dy=\"4.000000000000028\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">22,500</tspan></text><path fill=\"none\" stroke=\"#aaaaaa\" d=\"M64.015625,84.00000000000003H838.156\" stroke-width=\"0.5\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></path><text x=\"51.515625\" y=\"25.00000000000003\" text-anchor=\"end\" font=\"10px &quot;Arial&quot;\" stroke=\"none\" fill=\"#888888\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;\" font-size=\"12px\" font-family=\"sans-serif\" font-weight=\"normal\"><tspan dy=\"4.000000000000028\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">30,000</tspan></text><path fill=\"none\" stroke=\"#aaaaaa\" d=\"M64.015625,25.00000000000003H838.156\" stroke-width=\"0.5\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></path><text x=\"696.1205241494531\" y=\"273.5\" text-anchor=\"middle\" font=\"10px &quot;Arial&quot;\" stroke=\"none\" fill=\"#888888\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;\" font-size=\"12px\" font-family=\"sans-serif\" font-weight=\"normal\" transform=\"matrix(1,0,0,1,0,7)\"><tspan dy=\"4\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">2013</tspan></text><text x=\"351.8491058626974\" y=\"273.5\" text-anchor=\"middle\" font=\"10px &quot;Arial&quot;\" stroke=\"none\" fill=\"#888888\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;\" font-size=\"12px\" font-family=\"sans-serif\" font-weight=\"normal\" transform=\"matrix(1,0,0,1,0,7)\"><tspan dy=\"4\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\">2012</tspan></text><path fill=\"#579de7\" stroke=\"none\" d=\"M64.015625,219.05493333333334C85.65016767922235,219.56626666666668,128.91925303766703,222.62345,150.5537957168894,221.10026666666667C172.18833839611176,219.57708333333335,215.45742375455646,209.1355825136612,237.09196643377882,206.86946666666668C258.49135104040096,204.6279825136612,301.29012025364517,204.88215,322.6895048602673,203.06986666666666C344.0888894668894,201.25758333333332,386.8876586801336,194.9129178506375,408.2870432867557,192.3712C429.92158596597807,189.80155118397084,473.1906713244228,182.51721666666668,494.82521400364516,182.6244C516.4597566828675,182.73158333333333,559.7288420413122,204.18057122040074,581.3633847205346,193.22866666666667C602.7627693271567,182.39580455373408,645.5615385404009,101.94395359116024,666.960923147023,95.48533333333336C688.1251496810448,89.09768692449357,730.4536027490886,135.13802307692308,751.6178292831105,141.8436C773.2523719623329,148.69818974358975,816.5214573207776,147.7554,838.156,149.726L838.156,261L64.015625,261Z\" fill-opacity=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;\"></path><path fill=\"none\" stroke=\"#007cff\" d=\"M64.015625,219.05493333333334C85.65016767922235,219.56626666666668,128.91925303766703,222.62345,150.5537957168894,221.10026666666667C172.18833839611176,219.57708333333335,215.45742375455646,209.1355825136612,237.09196643377882,206.86946666666668C258.49135104040096,204.6279825136612,301.29012025364517,204.88215,322.6895048602673,203.06986666666666C344.0888894668894,201.25758333333332,386.8876586801336,194.9129178506375,408.2870432867557,192.3712C429.92158596597807,189.80155118397084,473.1906713244228,182.51721666666668,494.82521400364516,182.6244C516.4597566828675,182.73158333333333,559.7288420413122,204.18057122040074,581.3633847205346,193.22866666666667C602.7627693271567,182.39580455373408,645.5615385404009,101.94395359116024,666.960923147023,95.48533333333336C688.1251496810448,89.09768692449357,730.4536027490886,135.13802307692308,751.6178292831105,141.8436C773.2523719623329,148.69818974358975,816.5214573207776,147.7554,838.156,149.726\" stroke-width=\"3\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></path><circle cx=\"64.015625\" cy=\"219.05493333333334\" r=\"4\" fill=\"#007cff\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"150.5537957168894\" cy=\"221.10026666666667\" r=\"4\" fill=\"#007cff\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"237.09196643377882\" cy=\"206.86946666666668\" r=\"4\" fill=\"#007cff\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"322.6895048602673\" cy=\"203.06986666666666\" r=\"4\" fill=\"#007cff\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"408.2870432867557\" cy=\"192.3712\" r=\"4\" fill=\"#007cff\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"494.82521400364516\" cy=\"182.6244\" r=\"4\" fill=\"#007cff\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"581.3633847205346\" cy=\"193.22866666666667\" r=\"4\" fill=\"#007cff\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"666.960923147023\" cy=\"95.48533333333336\" r=\"4\" fill=\"#007cff\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"751.6178292831105\" cy=\"141.8436\" r=\"4\" fill=\"#007cff\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"838.156\" cy=\"149.726\" r=\"4\" fill=\"#007cff\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><path fill=\"#5d646a\" stroke=\"none\" d=\"M64.015625,240.02746666666667C85.65016767922235,239.8072,128.91925303766703,241.35496666666666,150.5537957168894,239.1464C172.18833839611176,236.93783333333334,215.45742375455646,223.33676429872497,237.09196643377882,222.35893333333334C258.49135104040096,221.39173096539162,301.29012025364517,233.23263333333333,322.6895048602673,231.36626666666666C344.0888894668894,229.4999,386.8876586801336,209.2890577413479,408.2870432867557,207.428C429.92158596597807,205.54649107468123,473.1906713244228,214.43916666666667,494.82521400364516,216.39600000000002C516.4597566828675,218.35283333333334,559.7288420413122,232.37947613843352,581.3633847205346,223.08266666666668C602.7627693271567,213.88690947176687,645.5615385404009,148.2268241252302,666.960923147023,142.42573333333334C688.1251496810448,136.68839079189686,730.4536027490886,170.47037838827842,751.6178292831105,176.92893333333336C773.2523719623329,183.53101172161175,816.5214573207776,190.23343333333335,838.156,194.66826666666668L838.156,261L64.015625,261Z\" fill-opacity=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;\"></path><path fill=\"none\" stroke=\"#495057\" d=\"M64.015625,240.02746666666667C85.65016767922235,239.8072,128.91925303766703,241.35496666666666,150.5537957168894,239.1464C172.18833839611176,236.93783333333334,215.45742375455646,223.33676429872497,237.09196643377882,222.35893333333334C258.49135104040096,221.39173096539162,301.29012025364517,233.23263333333333,322.6895048602673,231.36626666666666C344.0888894668894,229.4999,386.8876586801336,209.2890577413479,408.2870432867557,207.428C429.92158596597807,205.54649107468123,473.1906713244228,214.43916666666667,494.82521400364516,216.39600000000002C516.4597566828675,218.35283333333334,559.7288420413122,232.37947613843352,581.3633847205346,223.08266666666668C602.7627693271567,213.88690947176687,645.5615385404009,148.2268241252302,666.960923147023,142.42573333333334C688.1251496810448,136.68839079189686,730.4536027490886,170.47037838827842,751.6178292831105,176.92893333333336C773.2523719623329,183.53101172161175,816.5214573207776,190.23343333333335,838.156,194.66826666666668\" stroke-width=\"3\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></path><circle cx=\"64.015625\" cy=\"240.02746666666667\" r=\"4\" fill=\"#495057\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"150.5537957168894\" cy=\"239.1464\" r=\"4\" fill=\"#495057\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"237.09196643377882\" cy=\"222.35893333333334\" r=\"4\" fill=\"#495057\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"322.6895048602673\" cy=\"231.36626666666666\" r=\"4\" fill=\"#495057\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"408.2870432867557\" cy=\"207.428\" r=\"4\" fill=\"#495057\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"494.82521400364516\" cy=\"216.39600000000002\" r=\"4\" fill=\"#495057\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"581.3633847205346\" cy=\"223.08266666666668\" r=\"4\" fill=\"#495057\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"666.960923147023\" cy=\"142.42573333333334\" r=\"4\" fill=\"#495057\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"751.6178292831105\" cy=\"176.92893333333336\" r=\"4\" fill=\"#495057\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle><circle cx=\"838.156\" cy=\"194.66826666666668\" r=\"4\" fill=\"#495057\" stroke=\"#ffffff\" stroke-width=\"1\" style=\"-webkit-tap-highlight-color: rgba(0, 0, 0, 0);\"></circle></svg><div class=\"morris-hover morris-default-style\" style=\"left: 16.25px; top: 141px; display: none;\"><div class=\"morris-hover-row-label\">2011 Q1</div><div class=\"morris-hover-point\" style=\"color: #495057\">\n                        Item 1:\n                        2,666\n                    </div><div class=\"morris-hover-point\" style=\"color: #007cff\">\n                        Item 2:\n                        2,666\n                    </div>\n                    </div></div>\n                </div>\n            </div>\n        </section>\n    </div>"

/***/ }),

/***/ "./src/app/components/dashboards/dashboard/dashboard.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/components/dashboards/dashboard/dashboard.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkcy9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/dashboards/dashboard/dashboard.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/components/dashboards/dashboard/dashboard.component.ts ***!
  \************************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _router_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../router.animations */ "./src/app/router.animations.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
        this.alerts = [];
        this.sliders = [];
        this.sliders.push({
            imagePath: 'assets/images/slider1.jpg',
            label: 'First slide label',
            text: 'Nulla vitae elit libero, a pharetra augue mollis interdum.'
        }, {
            imagePath: 'assets/images/slider2.jpg',
            label: 'Second slide label',
            text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
        }, {
            imagePath: 'assets/images/slider3.jpg',
            label: 'Third slide label',
            text: 'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
        });
        this.alerts.push({
            id: 1,
            type: 'warning',
            message: "Your Subscription Will Expired On 31/12/2018."
        });
    }
    DashboardComponent.prototype.ngOnInit = function () { };
    DashboardComponent.prototype.closeAlert = function (alert) {
        var index = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    };
    DashboardComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/components/dashboards/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.scss */ "./src/app/components/dashboards/dashboard/dashboard.component.scss")],
            animations: [Object(_router_animations__WEBPACK_IMPORTED_MODULE_1__["routerTransition"])()]
        }),
        __metadata("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/app/components/dashboards/dashboard/dashboard.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/components/dashboards/dashboard/dashboard.module.ts ***!
  \*********************************************************************/
/*! exports provided: DashboardModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardModule", function() { return DashboardModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./dashboard-routing.module */ "./src/app/components/dashboards/dashboard/dashboard-routing.module.ts");
/* harmony import */ var _dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dashboard.component */ "./src/app/components/dashboards/dashboard/dashboard.component.ts");
/* harmony import */ var _shared_index__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../shared/index */ "./src/app/shared/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbCarouselModule"],
                _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbAlertModule"],
                _dashboard_routing_module__WEBPACK_IMPORTED_MODULE_3__["DashboardRoutingModule"],
                _shared_index__WEBPACK_IMPORTED_MODULE_5__["StatModule"]
            ],
            declarations: [
                _dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"],
            ]
        })
    ], DashboardModule);
    return DashboardModule;
}());



/***/ })

}]);
//# sourceMappingURL=dashboards-dashboard-dashboard-module.js.map