import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guard/';

const routes: Routes = [
   { path: '', loadChildren: './components/layout/layout.module#LayoutModule', canActivate: [AuthGuard] },
    { path: 'login', loadChildren: './components/auth/login/login.module#LoginModule' },
    { path: 'signup', loadChildren: './components/auth/signup/signup.module#SignupModule' },
    { path: 'forgot-password', loadChildren: './components/auth/forgot/forgot.module#ForgotModule' },
    { path: 'reset-password', loadChildren: './components/auth/reset/reset.module#ResetModule' },
    { path: 'error', loadChildren: './server-error/server-error.module#ServerErrorModule' },
    { path: 'access-denied', loadChildren: './access-denied/access-denied.module#AccessDeniedModule' },
    { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
