import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { AllUsersRoutingModule } from './all-users-routing.module';
import { AllUsersComponent } from './all-users.component';
import { StatModule } from '../../shared/index';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule,
        NgbAlertModule,
        AllUsersRoutingModule,
        StatModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule

    ],
    declarations: [
        AllUsersComponent
    ]
})
export class AllUsersModule {}
