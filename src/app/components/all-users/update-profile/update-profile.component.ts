import { Component, OnInit } from '@angular/core';
import {BsModalService} from 'ngx-bootstrap/modal';
import {AuthenticationService} from '../../../shared/services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../../shared/services/alert.service';

@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.component.html',
  styleUrls: ['./update-profile.component.scss']
})
export class UpdateProfileComponent implements OnInit {
    // public imagePath;
    // imgURL: any = 'assets/images/user.png';
    model: any = {};
    users: any = [];
    public message: string;
    imagedata: any = [];
    images: any = [];
    loading = false;
    profile_image: string;
    submitted = false;
    showImg: boolean = false;
    updateProfile: FormGroup;
  constructor(private alertService: AlertService,
              private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private authenticationService: AuthenticationService,
              public router: Router) {
      this.updateProfile = this.formBuilder.group({
          fname: ['', Validators.required],
          lname: ['', Validators.required],
          name: [''],
          email: ['', Validators.required]
      });
  }
    get f() { return this.updateProfile.controls; }

  ngOnInit() {
      this.userDetails();
      this.profile_image = localStorage.getItem('profile_photo');
  }

    public userDetails() {
        this.authenticationService.get('details')
            .subscribe(
                (response: Response) => {
                    this.users = response['result'];
                    this.model.email = response['result'].email;
                    this.model.fname = response['result'].fname;
                    this.model.lname = response['result'].lname;
                    this.model.name = response['result'].name;
                    localStorage.setItem('profile_photo', response['result'].profile_photo);
                }, error => {
                    this.loading = false;
                }
            );
    }

    onSubmit() {
        this.submitted = true;
        if (this.updateProfile.invalid) {
            return;
        }
        this.loading = true;
        const fd = new FormData();
        fd.append('fname', this.model.fname);
        fd.append('lname', this.model.lname);
        for (let i = 0; i < this.imagedata.length; i++) {
            fd.append('profile_image', this.imagedata[i]);
        }
        this.authenticationService.post('updateProfile', fd)
            .subscribe(
                (response: Response) => {
                    this.userDetails();
                }, error => {
                    this.loading = false;
                }
            );
    }

    imageFinishedUploading(event) {
        this.showImg = true;
        for (let i = 0; i < event.target.files.length; i++) {
            let name = 'mis_images[' + i + ']';
            this.getBase64(event.target.files[i]).then(
                data => {
                    this.images.push(data);
                }
            );
            this.imagedata.push(event.target.files[i]);
        }
    }
    getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    // preview(files) {
    //     if (files.length === 0)
    //         return;
    //
    //     const mimeType = files[0].type;
    //     if (mimeType.match(/image\/*/) == null) {
    //         this.message = "Only images are supported.";
    //         return;
    //     }
    //     const reader = new FileReader();
    //     this.imagePath = files;
    //     reader.readAsDataURL(files[0]);
    //     reader.onload = (_event) => {
    //         this.profile_image = reader.result;
    //     };
    // }
}
