import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { UpdateProfileRoutingModule } from './update-profile-routing.module';
import { UpdateProfileComponent } from './update-profile.component';
import { StatModule } from '../../../shared/index';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule,
        NgbAlertModule,
        UpdateProfileRoutingModule,
        StatModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule

    ],
    declarations: [
        UpdateProfileComponent
    ]
})
export class UdateProfileModule {}
