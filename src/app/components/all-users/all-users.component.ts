import { Component, OnInit } from '@angular/core';
import {BsModalService} from 'ngx-bootstrap/modal';
import {AuthenticationService} from '../../shared/services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})
export class AllUsersComponent implements OnInit {

    users: any = [];
    loading = false;

    constructor(private modalService: BsModalService,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                public router: Router) { }

    ngOnInit() {
        this.fetchBuildings();
    }

    public fetchBuildings() {
        this.authenticationService.get('fetchAllUsers')
            .subscribe(
                (response: Response) => {
                    this.users = response['result'];
                }, error => {
                    this.loading = false;
                }
            );
    }
}

