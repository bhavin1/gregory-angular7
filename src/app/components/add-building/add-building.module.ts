import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { AddBuildingRoutingModule } from './add_building-routing.module';
import { AddBuildingComponent } from './add-building.component';
import { StatModule } from '../../shared/index';
import { TranslateModule } from '@ngx-translate/core';
import {FormArray, FormBuilder , FormControl, FormGroup, Validators} from '@angular/forms';
import {ProgressBarModule} from 'angular-progress-bar';
import { EditBuildingComponent } from '../edit-building/edit-building.component';
@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule,
        NgbAlertModule,
        AddBuildingRoutingModule,
        StatModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        ProgressBarModule
    ],
    declarations: [
        AddBuildingComponent,
        EditBuildingComponent,
    ]
})
export class AddBuildingModule {}
