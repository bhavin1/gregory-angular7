import {Component, OnInit, Input, Output, EventEmitter, HostListener} from '@angular/core';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {BsModalRef} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../shared/services/authentication.service';
import {
    HttpClient, HttpResponse, HttpRequest, HttpEvent,
    HttpEventType, HttpErrorResponse
} from '@angular/common/http';
import {AppSettings} from '../../config/app.config';
import {map, debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

@Component({
    selector: 'app-add-building',
    templateUrl: './add-building.component.html',
    styleUrls: ['./add-building.component.scss']
})
export class AddBuildingComponent implements OnInit {
    public progress: any = [];
    fileToUpload: File = null;
    public imagePath;
    imgURL: any = 'assets/images/sample_building.jpg';
    public message: string;
    dragAreaClass: string = 'dragarea';
    imagedata: any = [];
    images: any = [];
    showImg: boolean = false;
    model: any = {};
    alert: any = {};
    addBuildingForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: any = [];
    percentDone: any;
    error = '';
    formData: FormData = new FormData();
    floors: FormArray;
    constructor(private modalService: BsModalService,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private translate: TranslateService,
                public router: Router,
                private http: HttpClient) {
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
        this.addBuildingForm = this.formBuilder.group({
            building_name: ['', Validators.required],
            owner_name: ['', Validators.required],
            owner_contact: ['', Validators.required],
            floor_counts: ['', Validators.required],
            building_address: ['', Validators.required],
            city: ['', Validators.required],
            state: ['', Validators.required],
            zip_code: ['', Validators.required],
            country: ['', Validators.required],
            coordinates: ['', Validators.required],
            floors: this.formBuilder.array([ this.createItem() ])
        });
    }

    initFloors() {
        return this.formBuilder.group({
            floor_description: ['', Validators.required],
            upload_files: ['', Validators.required],
        });
    }

    createItem(): FormGroup {
        return this.formBuilder.group({
            floor_description: ['', Validators.required],
            upload_files: ['', Validators.required],
        });
    }

    ngOnInit() {}

    get f() {
        return this.addBuildingForm.controls;
    }

    // image upload (dropdown)
    onFileChange(event) {
        const files = event.target.files;
        this.saveFiles(files);
    }

    @HostListener('dragover', ['$event']) onDragOver(event) {
        this.dragAreaClass = 'droparea';
        event.preventDefault();
    }

    @HostListener('dragenter', ['$event']) onDragEnter(event) {
        this.dragAreaClass = 'droparea';
        event.preventDefault();
    }

    @HostListener('dragend', ['$event']) onDragEnd(event) {
        this.dragAreaClass = 'dragarea';
        event.preventDefault();
    }

    @HostListener('dragleave', ['$event']) onDragLeave(event) {
        this.dragAreaClass = 'dragarea';
        event.preventDefault();
    }

    @HostListener('drop', ['$event']) onDrop(event) {
        this.dragAreaClass = 'dragarea';
        event.preventDefault();
        event.stopPropagation();
        const files = event.dataTransfer.files;
        this.saveFiles(files);
    }

    saveFiles(files) {
        this.preview(files);
        if (files.length > 0) {
            for (var j = 0; j < files.length; j++) {
                this.formData.append('building_image', files[j], files[j].name);
            }
        }
    }

    preview(files) {
        if (files.length === 0)
            return;

        const mimeType = files[0].type;
        if (mimeType.match(/image\/*/) == null) {
            this.message = 'Only images are supported.';
            return;
        }
        const reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
            this.imgURL = reader.result;
        };
    }
    // add card file code
    fileChange(event, index) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            const formData1: FormData = new FormData();
            formData1.append('cadd_file', file, file.name);
            const headers = new Headers();
            /** In Angular 5, including the header Content-Type can invalidate your request */
            headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');

            const req = new HttpRequest('POST', AppSettings.API_ENDPOINT + 'convertBuilding', formData1, {
                reportProgress: true,
            });
            this.http.request(req).subscribe((event: HttpResponse<any>) => {
                const res1 = this.getEventMessage(event, file, index);
                    if (event.body) {
                        if ( event.body.msg == 'Success') {
                            this.progress[index] = this.percentDone;
                            this.returnUrl[index] = event.body.result.urn;
                        }
                    }
                });
        }
    }

    private getEventMessage(event: HttpEvent<any>, file: File, index) {
        switch (event.type) {
            case HttpEventType.Sent:
                return `Uploading file "${file.name}" of size ${file.size}.`;

            case HttpEventType.UploadProgress:
                // Compute and show the % done:
                 this.percentDone = Math.round(100 * event.loaded / event.total);
                return `File "${file.name}" is ${this.percentDone}% uploaded.`;

            case HttpEventType.Response:
                return `File "${file.name}" was completely uploaded!`;

            default:
                return `File "${file.name}" surprising upload event: ${event.type}.`;
        }
    }

    addCreds(): void {
        this.floors = this.addBuildingForm.get('floors') as FormArray;
        this.floors.push(this.createItem());
    }

    removeCurrentField(index) {
        this.floors = this.addBuildingForm.get('floors') as FormArray;
        this.floors.removeAt(index);
    }

    onSubmit() {
        this.submitted = true;
        if (this.addBuildingForm.invalid) {
            return;
        }
        this.formData.append('building_name', this.model.building_name);
        this.formData.append('owner_name', this.model.owner_name);
        this.formData.append('owner_contact', this.model.owner_contact);
        this.formData.append('floor_counts', this.model.floor_counts);
        this.formData.append('city', this.model.city);
        this.formData.append('state', this.model.state);
        this.formData.append('zip_code', this.model.zip_code);
        this.formData.append('country', this.model.country);
        this.formData.append('coordinates', this.model.coordinates);
        this.loading = true;
        this.addBuildingForm.value.floors.forEach((item, index ) => {
            const data = {'floor_description': item.floor_description , 'urn': this.returnUrl[index]};
            this.formData.append('floor_data[]', JSON.stringify(data));
        });
        this.authenticationService.post('addBuilding', this.formData)
            .subscribe(
                (response: Response) => {
                    console.log(response);
                }, error => {
                    this.loading = false;
                }
            );
    }
}

