import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddBuildingComponent } from './add-building.component';

const routes: Routes = [
    {
        path: '', component: AddBuildingComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AddBuildingRoutingModule {
}
