import {Component, ElementRef, HostListener, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BsModalService} from 'ngx-bootstrap/modal';
import {HttpClient, HttpEvent, HttpEventType, HttpRequest, HttpResponse} from '@angular/common/http';
import {AuthenticationService} from '../../../shared/services/authentication.service';
import {TranslateService} from '@ngx-translate/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppSettings} from '../../../config/app.config';
import {AutodeskService} from '../../../shared/services/autodesk.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from '../../../shared/services/alert.service';
import {ConfirmationDialogService} from '../../layout/confirmation-dialog/confirmation-dialog.service';
declare const Autodesk: any;
@Component({
  selector: 'app-edit-building',
  templateUrl: './edit-building.component.html',
  styleUrls: ['./edit-building.component.scss']
})
export class EditBuildingComponent implements OnInit {
    model: any = {};
    floorData: any = [];
    public progress: any = [];
    fileToUpload: File = null;
    public imagePath;
    imgURL: any;
    // imgURL: any = 'assets/images/sample_building.jpg';
    public message: string;
    dragAreaClass: string = 'dragarea';
    imagedata: any = [];
    images: any = [];
    showImg: boolean = false;
    alert: any = {};
    editBuildingForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: any = [];
    percentDone: any;
    error = '';
    formData: FormData = new FormData();
    floors: FormArray;
    private viewer: any;
    urn: string;
    accessToken: any;
    expires_in = '5000';
  constructor(private alertService: AlertService,
              private confirmationDialogService: ConfirmationDialogService,
              private modalService: NgbModal,
              private route: ActivatedRoute,
              // private modalService: BsModalService,
              private formBuilder: FormBuilder,
              private authenticationService: AuthenticationService,
              private translate: TranslateService,
              public router: Router,
              private http: HttpClient,
              private elementRef: ElementRef,
              private autodeskService: AutodeskService) {
      this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
      this.translate.setDefaultLang('en');
      const browserLang = this.translate.getBrowserLang();
      this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
      this.editBuildingForm = this.formBuilder.group({
          building_name: ['', Validators.required],
          owner_name: ['', Validators.required],
          owner_contact: ['', Validators.required],
          floor_counts: ['', Validators.required],
          building_address: ['', Validators.required],
          city: ['', Validators.required],
          state: ['', Validators.required],
          zip_code: ['', Validators.required],
          country: ['', Validators.required],
          coordinates: ['', Validators.required],
          floors: this.formBuilder.array([ this.createItem() ])
      });
  }

  ngOnInit() {
      this.route.params.subscribe(params => {
          const id = params['id'];
          this.fetchBuildingsInfo(id);
      });
  }

    public fetchBuildingsInfo(id) {
        this.authenticationService.post('editBuilding', {'id': id } )
            .subscribe(
                (response: Response) => {
                    console.log(response['result']);
                    this.model.building_id = response['result'].id;
                    this.model.building_name = response['result'].building_name;
                    this.model.owner_name = response['result'].owner_name;
                    this.model.owner_contact = response['result'].owner_contact;
                    this.model.floor_counts = response['result'].floor_count;
                    this.model.building_address = response['result'].building_address;
                    this.model.city = response['result'].building_city;
                    this.model.state = response['result'].building_state;
                    this.model.zip_code = response['result'].building_zipcode;
                    this.model.country = response['result'].building_country;
                    this.model.coordinates = response['result'].building_coordinate;
                    this.imgURL = response['result'].building_image;
                    this.floorData = response['result'].floor_data;
                }, error => {
                    this.loading = false;
                }
            );
    }

    initFloors() {
        return this.formBuilder.group({
            floor_description: [''],
            upload_files: [''],
        });
    }

    createItem(): FormGroup {
        return this.formBuilder.group({
            floor_description: [''],
            upload_files: [''],
        });
    }

    get f() {
        return this.editBuildingForm.controls;
    }

    // image upload (dropdown)
    onFileChange(event) {
        const files = event.target.files;
        this.saveFiles(files);
    }

    @HostListener('dragover', ['$event']) onDragOver(event) {
        this.dragAreaClass = 'droparea';
        event.preventDefault();
    }

    @HostListener('dragenter', ['$event']) onDragEnter(event) {
        this.dragAreaClass = 'droparea';
        event.preventDefault();
    }

    @HostListener('dragend', ['$event']) onDragEnd(event) {
        this.dragAreaClass = 'dragarea';
        event.preventDefault();
    }

    @HostListener('dragleave', ['$event']) onDragLeave(event) {
        this.dragAreaClass = 'dragarea';
        event.preventDefault();
    }

    @HostListener('drop', ['$event']) onDrop(event) {
        this.dragAreaClass = 'dragarea';
        event.preventDefault();
        event.stopPropagation();
        const files = event.dataTransfer.files;
        this.saveFiles(files);
    }

    saveFiles(files) {
        this.preview(files);
        if (files.length > 0) {
            for (var j = 0; j < files.length; j++) {
                this.formData.append('building_image', files[j], files[j].name);
            }
        }
    }

    preview(files) {
        if (files.length === 0)
            return;

        const mimeType = files[0].type;
        if (mimeType.match(/image\/*/) == null) {
            this.message = 'Only images are supported.';
            return;
        }
        const reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
            this.imgURL = reader.result;
        };
    }
    // add card file code
    fileChange(event, index) {
        const fileList: FileList = event.target.files;
        if (fileList.length > 0) {
            const file: File = fileList[0];
            const formData1: FormData = new FormData();
            formData1.append('cadd_file', file, file.name);
            const headers = new Headers();
            /** In Angular 5, including the header Content-Type can invalidate your request */
            headers.append('Content-Type', 'multipart/form-data');
            headers.append('Accept', 'application/json');

            const req = new HttpRequest('POST', AppSettings.API_ENDPOINT + 'convertBuilding', formData1, {
                reportProgress: true,
            });
            this.http.request(req).subscribe((event: HttpResponse<any>) => {
                const res1 = this.getEventMessage(event, file, index);
                if (event.body) {
                    if ( event.body.msg == 'Success') {
                        this.progress[index] = this.percentDone;
                        this.returnUrl[index] = event.body.result.urn;
                    }
                }
            });
        }
    }

    private getEventMessage(event: HttpEvent<any>, file: File, index) {
        switch (event.type) {
            case HttpEventType.Sent:
                return `Uploading file "${file.name}" of size ${file.size}.`;

            case HttpEventType.UploadProgress:
                // Compute and show the % done:
                this.percentDone = Math.round(100 * event.loaded / event.total);
                return `File "${file.name}" is ${this.percentDone}% uploaded.`;

            case HttpEventType.Response:
                return `File "${file.name}" was completely uploaded!`;

            default:
                return `File "${file.name}" surprising upload event: ${event.type}.`;
        }
    }

    addCreds(): void {
        this.floors = this.editBuildingForm.get('floors') as FormArray;
        this.floors.push(this.createItem());
    }

    removeCurrentField(index) {
        this.floors = this.editBuildingForm.get('floors') as FormArray;
        this.floors.removeAt(index);
    }

    onSubmit() {
        this.submitted = true;
        if (this.editBuildingForm.invalid) {
            console.log(this.editBuildingForm);
            return;
        }
        this.formData.append('building_name', this.model.building_name);
        this.formData.append('owner_name', this.model.owner_name);
        this.formData.append('owner_contact', this.model.owner_contact);
        this.formData.append('floor_counts', this.model.floor_counts);
        this.formData.append('city', this.model.city);
        this.formData.append('state', this.model.state);
        this.formData.append('zip_code', this.model.zip_code);
        this.formData.append('country', this.model.country);
        this.formData.append('coordinates', this.model.coordinates);
        this.loading = true;
        this.editBuildingForm.value.floors.forEach((item, index ) => {
            const data = {'floor_description': item.floor_description , 'urn': this.returnUrl[index]};
            this.formData.append('floor_data[]', JSON.stringify(data));
        });
        this.authenticationService.post('addBuilding', this.formData)
            .subscribe(
                (response: Response) => {
                    console.log(response);
                }, error => {
                    this.loading = false;
                }
            );
    }

    deleteUploadedFile(id) {
        this.loading = true;
        this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to ... ?')
            .then((confirmed) => {
                if (confirmed) {
                    this.authenticationService.post('deleteBuilding', {'id': id } )
                        .subscribe(
                            (response: Response) => {
                                this.alertService.success(response['msg']);
                            }, error => {
                                this.loading = false;
                            }
                        );
                }
            })
            .catch(() => console.log('error'));
    }

    //autodesk code
    public getAccessToken() {
        this.autodeskService.getAccessToken('getForgeAutherization')
            .subscribe((response: Response)=>{
                this.accessToken = response['result'].access_token;
                console.log(this.accessToken);
                this.launchViewer();
            });
    }

    showModal(content, urn) {

        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {

        }, (reason) => {

        });

        this.urn = urn;
        if(!this.accessToken){
            this.getAccessToken();
        }else{
            this.launchViewer();
        }

    }

    private launchViewer() {

        if (this.viewer) {
            // Viewer has already been initialised
            //return;
        }

        const options = {
            env: 'AutodeskProduction',
            accessToken: this.accessToken
        };

        // For a headless viewer
        //this.viewer = new Autodesk.Viewing.Viewer3D(this.viewerContainer.nativeElement, {});
        let div = document.querySelector('#viewer_div');

        //this.viewer = new Autodesk.Viewing.Viewer3D(this.viewerContainer.nativeElement, {});
        this.viewer = new Autodesk.Viewing.Viewer3D(div, {});
        // For a viewer with UI
        // this.viewer = new Autodesk.Viewing.Private.GuiViewer3D(this.viewerContainer.nativeElement, {});

        Autodesk.Viewing.Initializer(options, () => {
            // Initialise the viewer and load a document
            this.viewer.initialize();
            this.loadDocument();
        });
    }

    private loadDocument() {
        const urn = 'urn:' + this.urn;

        Autodesk.Viewing.Document.load(urn, (doc) => {
            // Get views that can be displayed in the viewer
            const geometryItems = Autodesk.Viewing.Document.getSubItemsWithProperties(doc.getRootItem(), {type: 'geometry'}, true);

            if (geometryItems.length === 0) {
                return;
            }

            // Example of adding event listeners
            this.viewer.addEventListener(Autodesk.Viewing.GEOMETRY_LOADED_EVENT, this.geometryLoaded);
            this.viewer.addEventListener(Autodesk.Viewing.SELECTION_CHANGED_EVENT, (event) => this.selectionChanged(event));

            // Load view in to the viewer
            this.viewer.load(doc.getViewablePath(geometryItems[0]));
        }, errorMsg => console.error);
    }

    private geometryLoaded(event: any) {
        const viewer = event.target;

        viewer.removeEventListener(Autodesk.Viewing.GEOMETRY_LOADED_EVENT, this.geometryLoaded);

        // Example - set light preset and fit model to view
        viewer.setLightPreset(8);
        viewer.fitToView();
    }

    private selectionChanged(event: any) {
        const model = event.model;
        const dbIds = event.dbIdArray;

        // Get properties of object
        this.viewer.getProperties(dbIds[0], (props) => {
            // Do something with properties.
        });
    }


    ngOnDestroy() {
        // Clean up the viewer when the component is destroyed
        if (this.viewer && this.viewer.running) {
            this.viewer.removeEventListener(Autodesk.Viewing.SELECTION_CHANGED_EVENT, this.selectionChanged);
            this.viewer.tearDown();
            this.viewer.finish();
            this.viewer = null;
        }
    }



}



