import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditBuildingComponent } from './edit-building.component';

const routes: Routes = [
    {
        path: '', component: EditBuildingComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EditBuildingRoutingModule {
}
