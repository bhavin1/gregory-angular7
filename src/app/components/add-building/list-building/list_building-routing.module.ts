import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListBuildingComponent } from './list-building.component';

const routes: Routes = [
    {
        path: '', component: ListBuildingComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AddBuildingRoutingModule {
}
