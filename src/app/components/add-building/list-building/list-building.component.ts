import {Component, OnInit, ViewChild, ElementRef, OnDestroy, TemplateRef} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import {AuthenticationService} from '../../../shared/services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import {AutodeskService} from '../../../shared/services/autodesk.service';

declare const Autodesk: any;

@Component({
    selector: 'app-list-building',
    templateUrl: './list-building.component.html',
    styleUrls: ['./list-building.component.scss']
})

export class ListBuildingComponent implements OnInit {
    private viewer: any;
    buildings: any = [];
    loading = false;
    urn: string;
    accessToken: any;
    expires_in = '5000';
    modalRef: BsModalRef;

    @ViewChild('viewerContainer') viewerContainer: any;

    constructor(private modalService1: BsModalService,
                private modalService: NgbModal,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                public router: Router,
                private elementRef: ElementRef,
                private autodeskService: AutodeskService) {
    }

    ngAfterViewInit() {
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService1.show(template);
    }

    ngOnInit() {
        this.fetchBuildings();
    }

    public fetchBuildings() {
        this.authenticationService.get('fetchBuildings')
            .subscribe(
                (response: Response) => {
                    this.buildings = response['result'];

                    console.log(this.buildings);
                }, error => {
                    this.loading = false;
                }
            );
    }

    public getAccessToken() {
        this.autodeskService.getAccessToken('getForgeAutherization')
            .subscribe((response: Response) => {
                this.accessToken = response['result'].access_token;
                console.log(this.accessToken);
                this.launchViewer();
            });
    }

    showModal(content, urn) {

        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg'}).result.then((result) => {

        }, (reason) => {

        });

        this.urn = urn;
        if (!this.accessToken) {
            this.getAccessToken();
        } else {
            this.launchViewer();
        }

    }

    private launchViewer() {

        if (this.viewer) {
            // Viewer has already been initialised
            //return;
        }

        const options = {
            env: 'AutodeskProduction',
            accessToken: this.accessToken
        };

        // For a headless viewer
        //this.viewer = new Autodesk.Viewing.Viewer3D(this.viewerContainer.nativeElement, {});
        let div = document.querySelector('#viewer_div');

        //this.viewer = new Autodesk.Viewing.Viewer3D(this.viewerContainer.nativeElement, {});
        this.viewer = new Autodesk.Viewing.Viewer3D(div, {});
        // For a viewer with UI
        // this.viewer = new Autodesk.Viewing.Private.GuiViewer3D(this.viewerContainer.nativeElement, {});

        Autodesk.Viewing.Initializer(options, () => {
            // Initialise the viewer and load a document
            this.viewer.initialize();
            this.loadDocument();
        });
    }

    private loadDocument() {
        const urn = 'urn:' + this.urn;

        Autodesk.Viewing.Document.load(urn, (doc) => {
            // Get views that can be displayed in the viewer
            const geometryItems = Autodesk.Viewing.Document.getSubItemsWithProperties(doc.getRootItem(), {type: 'geometry'}, true);

            if (geometryItems.length === 0) {
                return;
            }

            // Example of adding event listeners
            this.viewer.addEventListener(Autodesk.Viewing.GEOMETRY_LOADED_EVENT, this.geometryLoaded);
            this.viewer.addEventListener(Autodesk.Viewing.SELECTION_CHANGED_EVENT, (event) => this.selectionChanged(event));

            // Load view in to the viewer
            this.viewer.load(doc.getViewablePath(geometryItems[0]));
        }, errorMsg => console.error);
    }

    private geometryLoaded(event: any) {
        const viewer = event.target;

        viewer.removeEventListener(Autodesk.Viewing.GEOMETRY_LOADED_EVENT, this.geometryLoaded);

        // Example - set light preset and fit model to view
        viewer.setLightPreset(8);
        viewer.fitToView();
    }

    private selectionChanged(event: any) {
        const model = event.model;
        const dbIds = event.dbIdArray;

        // Get properties of object
        this.viewer.getProperties(dbIds[0], (props) => {
            // Do something with properties.
        });
    }


    ngOnDestroy() {
        // Clean up the viewer when the component is destroyed
        if (this.viewer && this.viewer.running) {
            this.viewer.removeEventListener(Autodesk.Viewing.SELECTION_CHANGED_EVENT, this.selectionChanged);
            this.viewer.tearDown();
            this.viewer.finish();
            this.viewer = null;
        }
    }


}
