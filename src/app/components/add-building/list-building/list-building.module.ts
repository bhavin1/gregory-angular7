import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { AddBuildingRoutingModule } from './list_building-routing.module';
import { ListBuildingComponent } from './list-building.component';
import { StatModule } from '../../../shared/index';
import { TranslateModule } from '@ngx-translate/core';
// import { AutodeskComponent } from './autodesk/autodesk.component';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule,
        NgbAlertModule,
        AddBuildingRoutingModule,
        StatModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule

    ],
    declarations: [
        ListBuildingComponent
    ]
})
export class ListBuildingModule {}
