import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { HelpRoutingModule } from './help-routing.module';
import { HelpComponent } from './help.component';
import { StatModule } from '../../shared/index';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule,
        NgbAlertModule,
        HelpRoutingModule,
        StatModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule

    ],
    declarations: [
        HelpComponent
    ]
})
export class HelpModule {}
