import { Component, OnInit , TemplateRef } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { PageHeaderModule } from '../../shared/index';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { AlertService } from '../../shared/services/alert.service';
import { AlertComponent } from '../layout/alert/alert.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../shared/services/authentication.service';
import {NotificationService} from '../../shared/services/notification.service';
import {ActivatedRoute, Router} from '@angular/router';
import { ConfirmationDialogService } from '../layout/confirmation-dialog/confirmation-dialog.service';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.scss'],
  animations: [routerTransition()]
})
export class SubscriptionComponent implements OnInit {
    model: any = {};
    subscriptions: any = [];
    alert: any = {};
    subscription_price;
    token;
    role;
    subscription: FormGroup;
    payment: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    public alerts: Array<any> = [];
    // public sliders: Array<any> = [];
    modalRef: BsModalRef;

    constructor(private alertService: AlertService,
                private confirmationDialogService: ConfirmationDialogService,
                private modalService: BsModalService,
                public router: Router,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService) {
        this.alerts.push(
            {
                id: 1,
                type: 'warning',
                message: `Your Subscription Will Expired On 31/12/2018.`
            },
        );

        this.subscription = this.formBuilder.group({
            subscription_charge: ['', Validators.required],
            description: ['', Validators.required],
            subscription_type: ['', Validators.required]
          });
          this.payment = this.formBuilder.group({
            subscribe_amount: ['', Validators.required],
            cardNumber: ['', Validators.required],
            cardExpiry: ['', Validators.required],
             cardCVC: ['', Validators.required]
          });
    }

    ngOnInit() {
        this.token = localStorage.getItem('currentUser');
        this.role = localStorage.getItem('user_role');
        this.getSubcription();
    }

    get f() { return this.subscription.controls; }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }
    openModal1(template: TemplateRef<any>,value) {
    this.model.subscribe_amount = value;
        console.log(value);
        this.subscription_price=value;
        this.modalRef = this.modalService.show(template);
    }
    public getSubcription() {
        this.authenticationService.get('getSubscription')
            .subscribe(
                (response: Response) => {
                    this.subscriptions = response['result'];
                }, error => {
                    this.loading = false;
                }
            );
    }

    onSubmit() {
        this.submitted = true;
        if (this.subscription.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.post('setSubscription', this.model)
            .subscribe(
                (response: Response) => {
                }, error => {
                    this.loading = false;
                }
            );
    }


    performDeletion(id) {
        this.loading = true;
        this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to ... ?')
            .then((confirmed) => {
                if (confirmed) {
                    this.authenticationService.post('deleteSubscription', {subscription_id: id})
                        .subscribe(
                            (response: Response) => {
                                this.getSubcription();
                                this.alertService.success(response['msg']);
                            }, error => {
                                this.loading = false;
                            }
                        );
                }
            })
            .catch(() => console.log('error'));
    }

}
