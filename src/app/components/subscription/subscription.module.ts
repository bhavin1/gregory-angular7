import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { SubscriptionRoutingModule } from './subscription-routing.module';
import { SubscriptionComponent } from './subscription.component';
import { PageHeaderModule } from '../../shared/index';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConfirmationDialogComponent } from '../layout/confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from '../layout/confirmation-dialog/confirmation-dialog.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AlertComponent } from '../layout/alert/alert.component';
import { StatModule } from '../../shared/index';
// import {
//     TimelineComponent,
//     NotificationComponent,
//     ChatComponent
// } from './components';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AuthGuard} from '../../shared';
import {JwtInterceptor} from '../../shared/helpers/jwt.interceptor';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule,
        NgbAlertModule,
        SubscriptionRoutingModule,
        StatModule,
        NgbModule,
        PageHeaderModule, FormsModule, ReactiveFormsModule,
    ],
    declarations: [
        SubscriptionComponent,
        ConfirmationDialogComponent,
        AlertComponent,
    ],
    providers: [ConfirmationDialogService],
    entryComponents: [ ConfirmationDialogComponent ],
    exports: [ConfirmationDialogComponent]
})
export class SubscriptionModule {}
