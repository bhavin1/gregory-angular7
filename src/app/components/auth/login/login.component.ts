import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../../../router.animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../shared/services/authentication.service';
import {Config} from 'codelyzer';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    model: any = {};
    alert: any = {};
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    constructor(
        private translate: TranslateService,
        public router: Router,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService
        ) {
            this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
            this.translate.setDefaultLang('en');
            const browserLang = this.translate.getBrowserLang();
            this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', Validators.required],
            password: ['', Validators.required]
        });

        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }

    get f() { return this.loginForm.controls; }

    onSubmit() {
         this.submitted = true;
        if (this.loginForm.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.post('login', this.model)
            .subscribe(
                (response: Response) => {
                    if (response['status'] == 1) {
                        this.loading = false;
                       localStorage.setItem('currentUser', response['result']['token']);
                       localStorage.setItem('user_role', response['result']['user']['role']);
                       localStorage.setItem('user_id', response['result']['user']['id']);
                        localStorage.setItem('user_name', response['result']['user']['name']);
                        localStorage.setItem('fname', response['result']['user']['fname']);
                        localStorage.setItem('lname', response['result']['user']['lname']);
                        localStorage.setItem('email', response['result']['user']['email']);
                        localStorage.setItem('profile_photo', response['result']['user']['profile_photo']);
                        localStorage.setItem('isLoggedin', 'true');      // check login user
                       this.router.navigate(['dashboard4']);
                    } else {
                        this.loading = false;
                        this.alert.success = true;
                        this.alert.alert_type = 'alert-danger';
                        this.alert.title = 'Error';
                         this.alert.title = response['msg'];
                    }
                }, error => {
                    this.loading = false;
                }



            );
    }
}
