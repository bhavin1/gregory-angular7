import { Component, OnInit } from '@angular/core';
import {routerTransition} from '../../../router.animations';
import { TranslateService } from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { AuthenticationService } from '../../../shared/services/authentication.service';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss'],
  animations: [routerTransition()]
})
export class ForgotComponent implements OnInit {
    model: any = {};
    alert: any = {};
    ForgotForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
     public alerts: Array<any> = [];

    constructor(private translate: TranslateService,
                public router: Router,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService) {
        // this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        // this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');

        this.ForgotForm = this.formBuilder.group({
            email: ['', Validators.required],
          });
    }
    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }

    get f() { return this.ForgotForm.controls; }

    onSubmit() {
        this.submitted = true;
        if (this.ForgotForm.invalid) {
            return;
        }
        console.log(this.model);
        this.loading = true;
        this.authenticationService.post('sendOTP', this.model)
            .subscribe(
                (response: Response) => {
                    if(response.status==1){
                        this.alerts.push(
                            {
                                id: 1,
                                type: 'warning',
                                message: 'OTP sent to the provided email id.'
                            }
                        );
                        this.router.navigate(['reset-password']);
                    }else {
                        this.loading = false;
                        this.alert.success = true;
                        this.alert.alert_type = 'alert-danger';
                        this.alert.title = 'Error';
                         this.alert.title = response['msg'];
                    }
                    console.log(response);
                }, error => {
                    this.loading = false;
                }
            );
    }
}
