import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { ForgotRoutingModule } from './forgot-routing.module';
import { ForgotComponent } from './forgot.component';
import 'rxjs/operators';
@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        ForgotRoutingModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [ForgotComponent]
})
export class ForgotModule {}
