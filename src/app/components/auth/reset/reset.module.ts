import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { ResetRoutingModule } from './reset-routing.module';
import { ResetComponent } from './reset.component';
import 'rxjs/operators';
@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        ResetRoutingModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [ResetComponent]
})
export class ResetModule {}
