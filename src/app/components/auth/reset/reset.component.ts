import { Component, OnInit } from '@angular/core';
import { Router , ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { routerTransition } from '../../../router.animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../../../shared/services/authentication.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss'],
  animations: [routerTransition()]
})
export class ResetComponent implements OnInit {
    model: any = {};
    alert: any = {};
    ResetForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    constructor(
        private translate: TranslateService,
        public router: Router,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService
    ) {
        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');
    }

    ngOnInit() {
        this.ResetForm = this.formBuilder.group({
            token: ['', Validators.required],
            password: ['', Validators.required],
            email: ['', Validators.required]
        });

        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }

    get f() { return this.ResetForm.controls; }

    onSubmit() {
        this.submitted = true;
        if (this.ResetForm.invalid) {
            return;
        }
        console.log(this.model);
        this.loading = true;
        this.authenticationService.post('changeForgetPassword', this.model)
            .subscribe(
                (response: Response) => {
                    //console.log(response);
                    if(response.status==1){
                        
                        this.router.navigate(['login']);
                    }else {
                        this.loading = false;
                        this.alert.success = true;
                        this.alert.alert_type = 'alert-danger';
                        this.alert.title = 'Error';
                         this.alert.title = response['msg'];
                    }
                }, error => {
                    this.loading = false;
                }



            );
    }

}
