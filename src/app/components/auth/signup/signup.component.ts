import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { TranslateService } from '@ngx-translate/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { AuthenticationService } from '../../../shared/services/authentication.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    model: any = {};
    alert: any = {};
    signup: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    error = '';
    roles = [{
        id: 2,
        name: "Arichtect"
    }, {
        id: 3,
        name: "Landlord"
    }, {
        id: 4,
        name: "Broker"
    }];
    constructor(private translate: TranslateService,
                public router: Router,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService) {
        // this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        // this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');

        this.signup = this.formBuilder.group({
            fname: ['', Validators.required],
            lname: ['', Validators.required],
            email: ['', Validators.required],
            role: ['', Validators.required],
           
            password: ['', Validators.required],
            c_password: ['', Validators.required]}, {
            validator: this.MustMatch('password', 'c_password')
        });
    }

    MustMatch(controlName: string, matchingControlName: string) {
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];

            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }

            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({mustMatch: true});
            } else {
                matchingControl.setErrors(null);
            }
        };
    }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
         this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/dashboard';
    }

    get f() { return this.signup.controls; }

    onSubmit() {
        this.submitted = true;
        if (this.signup.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.post('register', this.model)
            .subscribe(
                (response: Response) => {
                    console.log(response);
                    if (response['status'] == 1) {
                        this.loading = false;
                        localStorage.setItem('currentUser', response['result']['token']);
                         localStorage.setItem('user_role', response['result']['user']['role']);
                        localStorage.setItem('user_id', response['result']['user']['id']);
                        localStorage.setItem('user_name', response['result']['user']['name']);
                        localStorage.setItem('fname', response['result']['user']['fname']);
                        localStorage.setItem('lname', response['result']['user']['lname']);
                        localStorage.setItem('email', response['result']['user']['email']);
                        localStorage.setItem('isLoggedin', 'true');      // check login user
                        this.router.navigate(['dashboard4']);
                    } else {
                        this.loading = false;
                        this.alert.success = true;
                        this.alert.alert_type = 'alert-danger';
                        this.alert.title = 'Error';
                        this.alert.title = response['msg'];
                    }
                }, error => {
                    this.loading = false;
                }
            );
    }
}
