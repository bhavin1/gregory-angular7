import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { EditBuildingRoutingModule } from './edit-building-routing.module';
import { EditBuildingComponent } from './edit-building.component';
import { StatModule } from '../../shared/index';
import { TranslateModule } from '@ngx-translate/core';
import {ProgressBarModule} from 'angular-progress-bar';
// import { AutodeskComponent } from './autodesk/autodesk.component';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule,
        NgbAlertModule,
        EditBuildingRoutingModule,
        StatModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        ProgressBarModule
    ],
    declarations: [
        EditBuildingComponent
    ]
})
export class EditBuildingModule {}
