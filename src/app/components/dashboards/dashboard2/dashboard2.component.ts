import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder} from '@angular/forms';
import {BsModalService} from 'ngx-bootstrap/modal';
import {AuthenticationService} from '../../../shared/services/authentication.service';

@Component({
  selector: 'app-dashboard2',
  templateUrl: './dashboard2.component.html',
  styleUrls: ['./dashboard2.component.scss']
})
export class Dashboard2Component implements OnInit {
 public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    buildings: any = [];
    loading = false;
    constructor(private modalService: BsModalService,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                public router: Router) {
        this.alerts.push(
            {
                id: 1,
                type: 'warning',
                message: `Your Subscription Will Expired On 31/12/2018.`
            },
        );
    }

    ngOnInit() {
        this.fetchBuildings();
    }

    public fetchBuildings() {
        this.authenticationService.get('fetchBuildings')
            .subscribe(
                (response: Response) => {
                    this.buildings = response['result'];
                }, error => {
                    this.loading = false;
                }
            );
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}