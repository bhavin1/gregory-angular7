import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { DashboardRoutingModule2 } from './dashboard2-routing.module';
import { Dashboard2Component } from './dashboard2.component';
import { PageHeaderModule } from '../../../shared/index';
// import {
//     TimelineComponent,
//     NotificationComponent,
//     ChatComponent
// } from './components';
import { StatModule } from '../../../shared/index';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule,
        NgbAlertModule,
        DashboardRoutingModule2,
        StatModule,
        PageHeaderModule
    ],
    declarations: [
        Dashboard2Component,
        // TimelineComponent,
        // NotificationComponent,
        // ChatComponent
    ]
})
export class DashboardModule2 {}
