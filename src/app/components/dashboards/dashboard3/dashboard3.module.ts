import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { PageHeaderModule } from '../../../shared/index';
import { DashboardRoutingModule3 } from './dashboard3-routing.module';
import { Dashboard3Component } from './dashboard3.component';
// import {
//     TimelineComponent,
//     NotificationComponent,
//     ChatComponent
// } from './components';
import { StatModule } from '../../../shared/index';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule,
        NgbAlertModule,
        DashboardRoutingModule3,
        StatModule,
        PageHeaderModule
    ],
    declarations: [
        Dashboard3Component,
        // TimelineComponent,
        // NotificationComponent,
        // ChatComponent
    ]
})
export class DashboardModule3 {}
