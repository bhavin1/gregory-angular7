import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Dashboard4Component } from './dashboard4.component';

const routes: Routes = [
    {
        path: '', component: Dashboard4Component
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule4 {
}
