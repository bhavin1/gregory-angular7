import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PageHeaderModule } from '../../../shared/index';
import {BsModalService} from 'ngx-bootstrap/modal';
import {AuthenticationService} from '../../../shared/services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {FormBuilder} from '@angular/forms';
@Component({
  selector: 'app-dashboard4',
  templateUrl: './dashboard4.component.html',
  styleUrls: ['./dashboard4.component.scss'],
  animations: [routerTransition()]
})
export class Dashboard4Component implements OnInit {

    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    buildings: any = [];
    user_count: any = [];
    loading = false;
    constructor(private modalService: BsModalService,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                public router: Router) {
        this.sliders.push(
            {
                imagePath: 'assets/images/slider1.jpg',
                label: 'First slide label',
                text:
                    'Nulla vitae elit libero, a pharetra augue mollis interdum.'
            },
            {
                imagePath: 'assets/images/slider2.jpg',
                label: 'Second slide label',
                text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'
            },
            {
                imagePath: 'assets/images/slider3.jpg',
                label: 'Third slide label',
                text:
                    'Praesent commodo cursus magna, vel scelerisque nisl consectetur.'
            }
        );

        this.alerts.push(
            {
                id: 1,
                type: 'warning',
                message: `Your Subscription Will Expired On 31/12/2018.`
            },
            // {
            //     id: 2,
            //     type: 'warning',
            //     message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            //     Voluptates est animi quibusdam praesentium quam, et perspiciatis,
            //     consectetur velit culpa molestias dignissimos
            //     voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
            // }
        );
    }

    ngOnInit() {
        this.fetchBuildings();
        this.userCounts();
    }

    public fetchBuildings() {
        this.authenticationService.get('fetchBuildings')
            .subscribe(
                (response: Response) => {
                    this.buildings = response['result'];
                }, error => {
                    this.loading = false;
                }
            );
    }

    public userCounts() {
        this.authenticationService.get('userCounts')
            .subscribe(
                (response: Response) => {
                    this.user_count.architect = response['result'][0].count;
                    this.user_count.landlord = response['result'][1].count;
                    this.user_count.broker = response['result'][2].count;
                    this.user_count.total = response['result'][0].count + response['result'][1].count + response['result'][2].count;
                }, error => {
                    this.loading = false;
                }
            );
    }

    public closeAlert(alert: any) {
        const index: number = this.alerts.indexOf(alert);
        this.alerts.splice(index, 1);
    }
}
