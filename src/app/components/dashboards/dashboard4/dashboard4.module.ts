import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';

import { DashboardRoutingModule4 } from './dashboard4-routing.module';
import { Dashboard4Component } from './dashboard4.component';
import { PageHeaderModule } from '../../../shared/index';
// import {
//     TimelineComponent,
//     NotificationComponent,
//     ChatComponent
// } from './components';
import { StatModule } from '../../../shared/index';

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule,
        NgbAlertModule,
        DashboardRoutingModule4,
        StatModule,
        PageHeaderModule
    ],
    declarations: [
        Dashboard4Component,
        // TimelineComponent,
        // NotificationComponent,
        // ChatComponent
    ]
})
export class DashboardModule4 {}
