import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            // { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: '.././dashboards/dashboard/dashboard.module#DashboardModule' },
            { path: 'dashboard2', loadChildren: '.././dashboards/dashboard2/dashboard2.module#DashboardModule2' },
            { path: 'dashboard3', loadChildren: '.././dashboards/dashboard3/dashboard3.module#DashboardModule3' },
            { path: 'dashboard4', loadChildren: '.././dashboards/dashboard4/dashboard4.module#DashboardModule4' },
            { path: 'add-building', loadChildren: '.././add-building/add-building.module#AddBuildingModule' },
            { path: 'edit-building/:id', loadChildren: '.././add-building/edit-building/edit-building.module#EditBuildingModule' },
            { path: 'all-users', loadChildren: '.././all-users/all-users.module#AllUsersModule' },
            { path: 'queries', loadChildren: '.././help/help.module#HelpModule' },
            { path: 'update-profile', loadChildren: '.././all-users/update-profile/update-profile.module#UdateProfileModule' },
            { path: 'list-building', loadChildren: '.././add-building/list-building/list-building.module#ListBuildingModule' },
            { path: 'subscription', loadChildren: '.././subscription/subscription.module#SubscriptionModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
          { path: 'edit-building', loadChildren: '.././edit-building/edit-building.module#EditBuildingModule' }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
