import {Component, OnInit, TemplateRef} from '@angular/core';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {BsModalRef} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthenticationService} from '../../../../shared/services/authentication.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    public imagePath;
    imgURL: any = 'assets/images/user.png';
    public message: string;
    model: any = {};  model2: any = {};
    showImg: boolean = false;
    fd = new FormData();
    imagedata: any = [];
    images: any = [];
    updateProfile: FormGroup;
    changePassword: FormGroup;
    users: any = [];
    loading = false;
    submitted = false;
    returnUrl: string;
    modalRef: BsModalRef;
    profile_image: string;

    public pushRightClass: string;
    constructor(private modalService: BsModalService,
                private formBuilder: FormBuilder,
                private route: ActivatedRoute,
                private authenticationService: AuthenticationService,
                private translate: TranslateService,
                public router: Router) {

        this.updateProfile = this.formBuilder.group({
            fname: ['', Validators.required],
            lname: ['', Validators.required],
            email: [''],
            profile_image: ['']
        });

        this.changePassword = this.formBuilder.group({
            new_password: ['', Validators.required],
            confirm_password: ['', Validators.required]}, {
            validator: this.MustMatch('new_password', 'confirm_password')
        });

        this.translate.addLangs(['en', 'fr', 'ur', 'es', 'it', 'fa', 'de', 'zh-CHS']);
        this.translate.setDefaultLang('en');
        const browserLang = this.translate.getBrowserLang();
        this.translate.use(browserLang.match(/en|fr|ur|es|it|fa|de|zh-CHS/) ? browserLang : 'en');

        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.userDetails();
        this.pushRightClass = 'push-right';
        this.profile_image = localStorage.getItem('profile_photo');

    }

    public userDetails() {
        this.authenticationService.get('details')
            .subscribe(
                (response: Response) => {
                    this.users = response['result'];
                    this.model.email = response['result'].email;
                    this.model.fname = response['result'].fname;
                    this.model.lname = response['result'].lname;
                    localStorage.setItem('profile_photo', response['result'].profile_photo);
                }, error => {
                    this.loading = false;
                }
            );
    }

    MustMatch(controlName: string, matchingControlName: string) {
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];

            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }

            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({mustMatch: true});
            } else {
                matchingControl.setErrors(null);
            }
        };
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    get f() { return this.updateProfile.controls; }

    onSubmit() {
        this.submitted = true;
        if (this.updateProfile.invalid) {
            return;
        }
        this.loading = true;
        const fd = new FormData();
        fd.append('fname', this.model.fname);
        fd.append('lname', this.model.lname);
        for (let i = 0; i < this.imagedata.length; i++) {
            fd.append('profile_image', this.imagedata[i]);
        }
        this.authenticationService.post('updateProfile', fd)
            .subscribe(
                (response: Response) => {
                    this.userDetails();
                 }, error => {
                    this.loading = false;
                }
            );
    }

    get c() { return this.changePassword.controls; }

    onSubmitChangePassword() {
        this.submitted = true;
        if (this.changePassword.invalid) {
            return;
        }
        this.loading = true;
        this.authenticationService.post('changePassword', this.model2)
            .subscribe(
                (response: Response) => {
                    console.log(response);
                }, error => {
                    this.loading = false;
                }
            );
    }

    imageFinishedUploading(event) {
        this.showImg = true;
        for (let i = 0; i < event.target.files.length; i++) {
            let name = 'mis_images[' + i + ']';
            this.getBase64(event.target.files[i]).then(
                data => {
                    this.images.push(data);
                }
            );
            this.imagedata.push(event.target.files[i]);
            console.log(this.imagedata[0] , 4);
        }
    }
    getBase64(file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
    }

    preview(files) {
        if (files.length === 0)
            return;

        const mimeType = files[0].type;
        if (mimeType.match(/image\/*/) == null) {
            this.message = "Only images are supported.";
            return;
        }
        const reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
            this.imgURL = reader.result;
        };
    }

}
