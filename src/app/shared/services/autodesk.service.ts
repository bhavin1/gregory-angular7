import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../config/app.config';
import 'rxjs/add/operator/map';
import { map, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import {NotificationService} from './notification.service';
import {ActivatedRoute, Router} from '@angular/router';
import { LoaderService } from './loader.service';
import { Observable } from 'rxjs/Observable';

@Injectable({providedIn: 'root'})
export class AutodeskService {

    constructor(private http: HttpClient,
        private notificationService: NotificationService,
        private router: Router,
        private service: LoaderService) {
    }

    getAccessToken(url) {
        return this.http.get<any>(AppSettings.API_ENDPOINT + url)
            .pipe(map((response: Response) => {
                    if (response) {
                        return response;
                    }
                })
            );
    }

}