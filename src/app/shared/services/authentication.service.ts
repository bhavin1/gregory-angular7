import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../config/app.config';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { LoaderService } from './loader.service';
import { Router } from '@angular/router';
import { map, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import {NotificationService} from './notification.service';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
    constructor(private http: HttpClient, private service: LoaderService,
                private router: Router, private notificationService: NotificationService) {
    }
    post(link, data, i = 0) {
        return this.http.post<any>(AppSettings.API_ENDPOINT + link, data)
            .pipe(map((response: Response) => {
                if (response) {
                    return response;
                } else {
                    this.service.emitConfig(false);
                }
            }), error => this.handleError = <any>error
    );
    }

    get(link, i = 0) {
        return this.http.get(AppSettings.API_ENDPOINT + link)
            .pipe(map((response: Response) => {
                if (response) {
                    return response;
                } else {
                    this.service.emitConfig(false);
                }
            }), error => this.handleError = <any>error);
    }

    logout() {
        localStorage.removeItem('isLoggedin');         // remove user from local storage to log user out
    }

    public handleError(error: Response) {

        let self = this;
        if (error.status === 401) {
            window.localStorage.clear();
            self.router.navigate(['/auth/login']);
            setTimeout(function() {
                self.notificationService.smallBox({
                    title: 'Error',
                    content: 'Session expired. Please login',
                    color: '#c46a69',
                    iconSmall: 'fa fa-check fa-2x fadeInRight animated',
                    timeout: 4000
                });
            }, 1000);
            // return Observable.throw(new Error(error.status));
        }

        if (error.status === 403) {
            self.notificationService.smallBox({
                title: 'Error',
                content: 'Access Denied',
                color: '#c46a69',
                iconSmall: 'fa fa-check fa-2x fadeInRight animated',
                timeout: 4000
            });
            // return Observable.throw(new Error(error.status));
        }
        self.service.emitConfig(false);
        return Observable.throw(error);
    }
}