import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule , HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared/guard';
import { SubscriptionModule} from './components/subscription/subscription.module'
import { ModalModule } from 'ngx-bootstrap/modal';
import { JwtInterceptor } from './shared/helpers/jwt.interceptor';
import { AlertService } from './shared/services/alert.service';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
export const createTranslateLoader = (http: HttpClient) => {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        ModalModule.forRoot(),
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        ModalModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule,
        AppRoutingModule,
        SlimLoadingBarModule.forRoot(),
        SubscriptionModule
    ],
    declarations: [AppComponent ],
    providers: [AuthGuard, { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true } ,
        AlertService ],
    bootstrap: [AppComponent],
})
export class AppModule {}
